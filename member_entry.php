<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Entry Page</title>
		<link rel="stylesheet" href="css/main.css" type="text/css" media="all" />
		<script type="text/javascript">
			function show_member(str)
			{
			if (str.length==0)
			  { 
			  document.getElementById("txtHint").innerHTML="";
			  return;
			  }
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			    {
			    document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
			    }
			  }
			xmlhttp.open("GET","entry/member_entry.php?q="+str,true);
			xmlhttp.send();
			}

			function isNumber(evt) {
				evt = (evt) ? evt : window.event;
				var charCode = (evt.which) ? evt.which : evt.keyCode;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					return false;
				}
				return true;
			}
		</script>
	</head>

	<body>
		
		<!-- header_start -->
		<?php include_once "templates/header_template.php"; ?>
		<!-- header_end -->
		
		<!-- Content_starts -->
		<div id="container">
			<div class="form_title">
				<h2>Member Entry</h2><br/><hr>
			</div>
			
			<center>
			<form name="lock_member_form" id="lock_member_form" method="post" action="member_entry.php">
			<table border="0">
				<tr>
					<td>Member ID</td>
					<td><input type="text" name="member_id" placeholder=" Member Id" onClick="show_member(this.value)" onkeyup="show_member(this.value)" value="" onkeypress="return isNumber(event)"/></td>
				</tr>
			</table>
			</form>
<?php
	if (isset($_GET['member_id']))
	{
		$in=$_GET['in'];
		$member_id=$_GET['member_id'];
		
		include 'db_config/db_config.php';
		$select = "SELECT * FROM sc_member WHERE member_id='$member_id'";
		$result = mysql_query($select) or die("ERROR 1 : ".mysql_error());
		while($rows=mysql_fetch_array($result))
		{
			$member_name = $rows['member_name'];
		}

		if($in==-1)
		{	
			echo "<br>Registration completed successfully ! Thank you !<br>";	
			echo "<br>Welcome, <a href='view_member_details.php?member_id=".$member_id."'>".$member_name."</a> !";
			echo "<br><br><a href='http://localhost/study_center/member/select_member_photo.php?member_id=".$member_id."'>Upload Photo</a>";
		}
		if($in==-2)
		{
			echo "<br>Fees renewed successfully for member <a href='view_member_details.php?member_id=".$member_id."'>".$member_name."</a>.<br>";
		}
		if($in==1)
		{
			echo "<br>Member <a href='view_member_details.php?member_id=".$member_id."'>".$member_name."</a> is currently Logged <b>In</b>";
		}
		else if($in==0)
		{
			echo "<br>Member <a href='view_member_details.php?member_id=".$member_id."'>".$member_name."</a> is currently Logged <b>Out</b>";
		}
	}
?>
			<span id="txtHint">

			</span>
			</center>
		</div>
		<!-- Content_end -->

		<!-- Footer_start -->
		<?php //include_once "templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>