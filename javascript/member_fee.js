function validate()
{
	var memberid=document.forms["member_fee_form"]["member_id"];
	var feeplan=document.forms["member_fee_form"]["fee_plan"];
	var bookletno=document.forms["member_fee_form"]["booklet_no"];
	var receiptno=document.forms["member_fee_form"]["receipt_no"];
		
	if(isNumeric(memberid, "Please enter only numbers for Member Id"))
	{
	if(isOptionSelected(feeplan, "Please select Fee Plan"))
	{
	if(isNumeric(bookletno, "Please enter a valid Booklet Number"))
	{
	if(isNumeric(receiptno, "Please enter a valid Receipt no"))
	{
		return true;
	}}}}
		return false;
}//end of function validate()

function notEmpty(element, message)
{
	if(element.value.length == 0)
	{
		alert(message);
		element.focus();
		return false;
	}
	return true;
}

function isAlphabet(element, message)
{
	var alphaExp = /^[a-zA-Z]+$/;
	if(element.value.match(alphaExp))
	{
		return true;
	}
	else
	{	alert(message);
		element.value="";
		element.focus();
		return false;
	}
}//end of function isAlphabet()

function isEmptyAlphabet(element, message)
{
	var alphaExp = /^[a-zA-Z]+$/;
	var element1 = element.value;
	if(element.value.match(alphaExp)||element1=='')
	{
		return true;
	}
	else
	{
		alert(message);
		element.focus();
		return false;
	}
}

function isOptionSelected(element,message)
{
	if(element.value==0)
	{
		alert(message);
		element.focus();
		return false;
	}
	else 
	{
		return true;
	}
}////end of function isOptionSelected()

function isRadiobuttonClicked(element,message)
{
	if ((member_form.gender[0].checked== false) && (member_form.gender[1].checked== false) )
	{
		alert(message);
		return false;
	}
	else
	{
		return true;
	}
}////end of function isRadiobuttonClicked()

function isAlphanumeric(element,message)
{
	var alphanumExp = /^[0-9a-zA-Z\s,.-]+$/;
	if(element.value.match(alphanumExp))
	{
		return true;
	}
	else
	{
		alert(message);
		element.focus();
		return false;
	}
}////end of function isAlphanumeric()

function isEmptyAlphanumeric(element, message)
{
	var alphanumExp = /^[0-9a-zA-Z\s,.-]+$/;
	var element1 = element.value;
	if(element.value.match(alphanumExp)||element1=='')
	{
		return true;
	}
	else
	{
		alert(message);
		element.focus();
		return false;
	}
}

function isNumeric(element,message)
{
	var numExp=/^[0-9]+$/;
	if(element.value.match(numExp))
	{
		return true;
	}
	else
	{
		alert(message);
		element.value="";
		element.focus();
		return false;
	}
}////end of function isNumeric()

function isEmptyNumeric(element, message)
{
	var numExp = /^[0-9]+$/;
	var element1 = element.value;
	if(element.value.match(numExp)||element1=='')
	{
		return true;
	}
	else
	{
		alert(message);
		element.focus();
		return false;
	}
}

function lengthRestriction(element, upperlimit, message)
{
	var  len=element.value;
	var fieldname=element;
	if(len.length == upperlimit)
	{
		return true;
	}
	else if(len.length != upperlimit)
	{
		alert(message);
		element.value="";
		element.focus();
		return false;
	}
}////end of function lengthRestriction()

function emailValidator(element, message)
{
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(element.value.match(emailExp))
	{
		return true;
	}
	else
	{
		alert(message);
		element.value="";
		element.focus();
		return false;
	}
}//end of function emailValidator()