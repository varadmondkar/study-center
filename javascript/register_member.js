function validate()
{
	var libraryid=document.forms["add_member_form"]["library_id"];
	var firstname=document.forms["add_member_form"]["first_name"];
	var middlename=document.forms["add_member_form"]["middle_name"];
	var lastname=document.forms["add_member_form"]["last_name"];
	var day=document.forms["add_member_form"]["day"];
	var month=document.forms["add_member_form"]["month"];
	var year=document.forms["add_member_form"]["year"];
	var gender=document.forms["add_member_form"]["gender"];
	var bloodgroup=document.forms["add_member_form"]["blood_group"];
	var add1=document.forms["add_member_form"]["add1"];
	var add2=document.forms["add_member_form"]["add2"];
	var add3=document.forms["add_member_form"]["add3"];
	var add4=document.forms["add_member_form"]["add4"];
	var city=document.forms["add_member_form"]["city"];
	var state=document.forms["add_member_form"]["state"];
	var pincode=document.forms["add_member_form"]["pincode"];
	var mobileno=document.forms["add_member_form"]["mobile_no"];
	var landlineno=document.forms["add_member_form"]["landline_no"];
	var emailid=document.forms["add_member_form"]["email_id"];
	var course=document.forms["add_member_form"]["course"];
	//var branch=document.forms["add_member_form"]["branch"];
	//var studyyear=document.forms["add_member_form"]["study_year"];
	var college=document.forms["add_member_form"]["college"];
	var university=document.forms["add_member_form"]["university"];
	var company=document.forms["add_member_form"]["company"];
	var designation=document.forms["add_member_form"]["designation"];
	var domain=document.forms["add_member_form"]["domain"];
	var companyaddress=document.forms["add_member_form"]["company_address"];
	var companytelephoneno=document.forms["add_member_form"]["company_telephone_no"];
	var interest=document.forms["add_member_form"]["interest"];
	var memberoforg=document.forms["add_member_form"]["member_of_org"];
	
	
	if(isEmptyNumeric(libraryid, "Please enter only numbers for Library Id"))
	{
	if(isAlphabet(firstname, "Please enter only alphabets for Firstname"))
	{
	if(isEmptyAlphabet(middlename, "Please enter only alphabets for Middlename"))
	{
	if(isAlphabet(lastname, "Please enter only alphabets for Lastname"))
	{
	if(isOptionSelected(day, "Please select Day of Date Of Birth"))
	{
	if(isOptionSelected(month, "Please select Month of Date Of Birth"))
	{
	if(isOptionSelected(year, "Please select Year of Date Of Birth"))
	{
	if(checkDate(day,month,year))
	{
	if(isOptionSelected(gender, "Please select Gender"))
	{
	if(isOptionSelected(bloodgroup, "Please select Blood Group"))
	{
	if(isEmptyAlphanumeric(add1, "Please enter only Numbers and Letters for House no"))
	{
	if(isAlphanumeric(add2, "Please enter only Numbers and Letters for Building name"))
	{
	if(isAlphanumeric(add3, "Please enter only Numbers and Letters for Street/Road"))
	{
	// if(isAlphanumeric(add4, "Please enter only Numbers and Letters for Landmark/Area"))
	// {
	if(isOptionSelected(city, "Please select City"))
	{
	if(isOptionSelected(state, "Please select State"))
	{
	if(isNumeric(pincode, "Please enter a valid pincode"))
	{
	if(lengthRestriction(pincode, 6, "Please enter 6 digit Pincode Number"))
	{
	if(isNumeric(mobileno, "Please enter a valid Mobile Number"))
	{
	if(lengthRestriction(mobileno, 10, "Please enter 10 digit Mobile Number"))
	{
	if(isEmptyNumeric(landlineno, "Please enter a valid Landline Number"))
	{
	if(emailValidator(emailid, "Please enter a valid Email Address"))
	{
	if(isOptionSelected(course, "Please select Course"))
	{
	// if(isOptionSelected(branch, "Please select Branch"))
	// {
	// if(isOptionSelected(studyyear, "Please select Study_year"))
	// {
	if(isOptionSelected(college, "Please select College"))
	{
	if(isOptionSelected(university, "Please select University"))
	{	
	if(isEmptyAlphanumeric(company, "Please enter only Numbers and Letters for Company name"))
	{
	if(isEmptyAlphanumeric(designation, "Please enter only Numbers and Letters for Designation"))
	{
	if(isEmptyAlphanumeric(domain, "Please enter only Numbers and Letters for Domain of Work"))
	{
	if(isEmptyAlphanumeric(companyaddress, "Please enter only Numbers and Letters for Company Address"))
	{
	if(isEmptyNumeric(companytelephoneno, "Please enter a valid Comapny Telephone no"))
	{
	if(isEmptyAlphanumeric(interest, "Please enter only Numbers and Letters for Interest"))
	{
	if(isEmptyAlphanumeric(memberoforg, "Please enter only Numbers and Letters for Member of Organisation"))
	{
		return true;
	}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}//}}}
		return false;
}//end of function validate()

function notEmpty(element, message)
{
	if(element.value.length == 0)
	{
		alert(message);
		element.focus();
		return false;
	}
	return true;
}

function isAlphabet(element, message)
{
	var alphaExp = /^[a-zA-Z]+$/;
	if(element.value.match(alphaExp))
	{
		return true;
	}
	else
	{	alert(message);
		element.value="";
		element.focus();
		return false;
	}
}//end of function isAlphabet()

function isEmptyAlphabet(element, message)
{
	var alphaExp = /^[a-zA-Z]+$/;
	var element1 = element.value;
	if(element.value.match(alphaExp)||element1=='')
	{
		return true;
	}
	else
	{
		alert(message);
		element.focus();
		return false;
	}
}

function isOptionSelected(element,message)
{
	if(element.value==0)
	{
		alert(message);
		element.focus();
		return false;
	}
	else 
	{
		return true;
	}
}////end of function isOptionSelected()

function isRadiobuttonClicked(element,message)
{
	if ((member_form.gender[0].checked== false) && (member_form.gender[1].checked== false) )
	{
		alert(message);
		return false;
	}
	else
	{
		return true;
	}
}////end of function isRadiobuttonClicked()

function isAlphanumeric(element,message)
{
	var alphanumExp = /^[0-9a-zA-Z\s,./-]+$/;
	if(element.value.match(alphanumExp))
	{
		return true;
	}
	else
	{
		alert(message);
		element.focus();
		return false;
	}
}////end of function isAlphanumeric()

function isEmptyAlphanumeric(element, message)
{
	var alphanumExp = /^[0-9a-zA-Z\s,./-]+$/;
	var element1 = element.value;
	if(element.value.match(alphanumExp)||element1=='')
	{
		return true;
	}
	else
	{
		alert(message);
		element.focus();
		return false;
	}
}

function isNumeric(element,message)
{
	var numExp=/^[0-9]+$/;
	if(element.value.match(numExp))
	{
		return true;
	}
	else
	{
		alert(message);
		element.value="";
		element.focus();
		return false;
	}
}////end of function isNumeric()

function isEmptyNumeric(element, message)
{
	var numExp = /^[0-9]+$/;
	var element1 = element.value;
	if(element.value.match(numExp)||element1=='')
	{
		return true;
	}
	else
	{
		alert(message);
		element.focus();
		return false;
	}
}

function lengthRestriction(element, upperlimit, message)
{
	var  len=element.value;
	var fieldname=element;
	if(len.length == upperlimit)
	{
		return true;
	}
	else if(len.length != upperlimit)
	{
		alert(message);
		element.value="";
		element.focus();
		return false;
	}
}////end of function lengthRestriction()

function emailValidator(element, message)
{
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(element.value.match(emailExp))
	{
		return true;
	}
	else
	{
		alert(message);
		element.value="";
		element.focus();
		return false;
	}
}//end of function emailValidator()

function checkDate(element1,element2,element3)
{
	var day=element1.value;
	var month=element2.value;
	var year=element3.value;
	
	if((month==04||month==06||month==09||month==11)&&(day==31)){
		alert("Entered month can not have day 31.");
		element2.focus();
		return false;
	}
	else if(!(((0==year%4)&&(0!=year%100))||(0==year%400))&&(month==02)&&(day==29||day==30||day==31)){
		alert(year+" is not a leap year it can not have day 29 or 30 or 31 for february.");
		element1.focus();
		return false;
	}
	else if((month==02)&&(day==30||day==31)){
		alert("February can not have day 30 , 31.");
		element2.focus();
		return false;
	}
	else
	{
		return true;
	}
}