SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `study_center` DEFAULT CHARACTER SET latin1 ;
USE `study_center` ;

-- -----------------------------------------------------
-- Table `study_center`.`sc_admin_rights`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `study_center`.`sc_admin_rights` (
  `admin_rights_id` INT(1) NOT NULL AUTO_INCREMENT ,
  `admin_type` VARCHAR(20) NOT NULL ,
  `entry` INT(1) NOT NULL ,
  `member` INT(1) NOT NULL ,
  `fee` INT(1) NOT NULL ,
  `user` INT(1) NOT NULL ,
  `update_db` INT(1) NOT NULL ,
  `update_user_rights` INT(1) NOT NULL ,
  `report` INT(1) NOT NULL ,
  PRIMARY KEY (`admin_rights_id`) ,
  INDEX `admin_rights_id` (`admin_rights_id` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `study_center`.`sc_security_question`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `study_center`.`sc_security_question` (
  `sc_security_question_id` INT(5) NOT NULL AUTO_INCREMENT ,
  `security_question` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`sc_security_question_id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `study_center`.`sc_admin`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `study_center`.`sc_admin` (
  `admin_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `admin_user_name` VARCHAR(30) NOT NULL ,
  `admin_password` VARCHAR(50) NOT NULL ,
  `admin_name` VARCHAR(50) NOT NULL ,
  `admin_address` VARCHAR(200) NULL DEFAULT NULL ,
  `admin_mobile_number` VARCHAR(12) NULL DEFAULT NULL ,
  `admin_residence_number` VARCHAR(12) NULL DEFAULT NULL ,
  `admin_email_id` VARCHAR(50) NULL DEFAULT NULL ,
  `admin_gender` VARCHAR(7) NULL DEFAULT NULL ,
  `admin_blood_group` VARCHAR(10) NULL DEFAULT NULL ,
  `admin_dob` DATE NULL DEFAULT NULL ,
  `admin_type` INT(1) NOT NULL ,
  `security_question_id` INT(5) NOT NULL ,
  `security_answer` VARCHAR(50) NOT NULL ,
  PRIMARY KEY (`admin_id`) ,
  INDEX `admin_type` (`admin_type` ASC) ,
  INDEX `security_question_id` (`security_question_id` ASC) ,
  INDEX `security_question_id_2` (`security_question_id` ASC) ,
  CONSTRAINT `admin_type`
    FOREIGN KEY (`admin_type` )
    REFERENCES `study_center`.`sc_admin_rights` (`admin_rights_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `security_question`
    FOREIGN KEY (`security_question_id` )
    REFERENCES `study_center`.`sc_security_question` (`sc_security_question_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `study_center`.`sc_branch`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `study_center`.`sc_branch` (
  `branch_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `branch_name` VARCHAR(80) NOT NULL ,
  PRIMARY KEY (`branch_id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `study_center`.`sc_college`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `study_center`.`sc_college` (
  `college_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `college_name` VARCHAR(150) NOT NULL ,
  PRIMARY KEY (`college_id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `study_center`.`sc_entry_log`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `study_center`.`sc_entry_log` (
  `entry_id` INT(15) NOT NULL AUTO_INCREMENT ,
  `entry_member_id` INT(11) NOT NULL ,
  `in_time` TIME NOT NULL ,
  `out_time` TIME NOT NULL ,
  `entry_date` DATE NOT NULL ,
  `admin_id` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`entry_id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `study_center`.`sc_member`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `study_center`.`sc_member` (
  `member_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `member_name` VARCHAR(100) NOT NULL ,
  `member_library_id` INT(11) NULL DEFAULT NULL ,
  `member_address` VARCHAR(500) NOT NULL ,
  `member_city` VARCHAR(30) NOT NULL ,
  `member_state` VARCHAR(30) NOT NULL ,
  `member_residence_number` VARCHAR(12) NULL DEFAULT NULL ,
  `member_mobile_number` VARCHAR(12) NOT NULL ,
  `member_email_id` VARCHAR(50) NOT NULL ,
  `member_gender` VARCHAR(7) NOT NULL ,
  `member_blood_group` VARCHAR(10) NOT NULL ,
  `member_type` VARCHAR(15) NULL DEFAULT NULL ,
  `member_dob` DATE NOT NULL ,
  `member_course` VARCHAR(20) NOT NULL ,
  `member_year` VARCHAR(20) NULL DEFAULT NULL ,
  `member_branch_name` VARCHAR(100) NOT NULL ,
  `member_college_name` VARCHAR(200) NOT NULL ,
  `member_university_name` VARCHAR(100) NOT NULL ,
  `member_company_name` VARCHAR(100) NULL DEFAULT NULL ,
  `member_company_designation` VARCHAR(50) NULL DEFAULT NULL ,
  `member_company_address` VARCHAR(200) NULL DEFAULT NULL ,
  `member_company_domain` VARCHAR(50) NULL DEFAULT NULL ,
  `member_company_telephone` VARCHAR(15) NULL DEFAULT NULL ,
  `member_status` INT(1) NULL DEFAULT NULL ,
  `member_reffered_by` VARCHAR(20) NULL DEFAULT NULL ,
  `member_interest` VARCHAR(50) NULL DEFAULT NULL ,
  `member_wish_to_participate_in_vsm` VARCHAR(5) NULL DEFAULT NULL ,
  `member_of_other_org` VARCHAR(20) NOT NULL DEFAULT 'No' ,
  `member_photo` VARCHAR(50) NULL DEFAULT NULL ,
  `admin_id` INT(10) NULL DEFAULT NULL ,
  PRIMARY KEY (`member_id`, `member_of_other_org`) )
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `study_center`.`sc_fee`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `study_center`.`sc_fee` (
  `fee_member_id` INT(11) NOT NULL ,
  `fee_registration_amount` TINYINT(2) NOT NULL DEFAULT '50' ,
  `fee_plan` VARCHAR(12) NOT NULL ,
  `fee_receipt_number` INT(10) NOT NULL ,
  `fee_booklet_number` INT(10) NOT NULL ,
  `fee_payment_date` DATE NOT NULL ,
  `fee_due_date` DATE NOT NULL ,
  PRIMARY KEY (`fee_member_id`) ,
  CONSTRAINT `fee_member_id`
    FOREIGN KEY (`fee_member_id` )
    REFERENCES `study_center`.`sc_member` (`member_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `study_center`.`sc_fee_history`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `study_center`.`sc_fee_history` (
  `fee_history_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `history_member_id` INT(11) NOT NULL ,
  `fee_plan` VARCHAR(20) NULL DEFAULT NULL ,
  `fee_amount` INT(5) NULL DEFAULT NULL ,
  `fee_receipt_number` INT(10) NULL DEFAULT NULL ,
  `fee_booklet_number` INT(10) NULL DEFAULT NULL ,
  `renew_date` DATE NULL DEFAULT NULL ,
  `time` TIME NULL DEFAULT NULL ,
  `due_date` DATE NULL DEFAULT NULL ,
  `admin_id` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`fee_history_id`) ,
  INDEX `history_member_id` (`history_member_id` ASC) ,
  CONSTRAINT `history_member_id`
    FOREIGN KEY (`history_member_id` )
    REFERENCES `study_center`.`sc_member` (`member_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `study_center`.`sc_fee_structure`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `study_center`.`sc_fee_structure` (
  `fee_structure_id` INT(3) NOT NULL ,
  `fee_amount` INT(6) NOT NULL ,
  `fee_duration` VARCHAR(15) NOT NULL ,
  PRIMARY KEY (`fee_structure_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `study_center`.`sc_member_flag`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `study_center`.`sc_member_flag` (
  `member_flag_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `registration_flag` TINYINT(1) NOT NULL ,
  `fee_flag` TINYINT(1) NOT NULL ,
  `lock_flag` TINYINT(1) NOT NULL ,
  `entry_flag` TINYINT(1) NOT NULL ,
  `member_notes` TEXT NULL DEFAULT NULL ,
  `lock_reason` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`member_flag_id`) ,
  INDEX `member_flag_id` (`member_flag_id` ASC) ,
  CONSTRAINT `member_flag_id`
    FOREIGN KEY (`member_flag_id` )
    REFERENCES `study_center`.`sc_member` (`member_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `study_center`.`sc_university`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `study_center`.`sc_university` (
  `university_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `university_name` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`university_id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
