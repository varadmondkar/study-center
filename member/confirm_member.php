<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}

	$library_id=$_POST['library_id'];
	$first_name=$_POST['first_name'];
	$middle_name=$_POST['middle_name'];
	$last_name=$_POST['last_name'];
	$day=$_POST['day'];
	$month=$_POST['month'];
	$year=$_POST['year'];
	$dob=$year."-".$month."-".$day;
	$gender=$_POST['gender'];
	$blood_group=$_POST['blood_group'];
	$add1=$_POST['add1'];
	$add2=$_POST['add2'];
	$add3=$_POST['add3'];
	$add4=$_POST['add4'];
	$city=$_POST['city'];
	$state=$_POST['state'];
	$pincode=$_POST['pincode'];
	$address=$add1.", ".$add2.", ".$add3.", ".$add4.", ".$city.", ".$state.", ".$pincode;
	$mobile=$_POST['mobile_no'];
	$landline=$_POST['landline_no'];
	$email_id=$_POST['email_id'];
	$course=$_POST['course'];
	$branch=$_POST['branch'];
	$study_year=$_POST['study_year'];
	$college=$_POST['college'];
	$university=$_POST['university'];
	$company=$_POST['company'];
	$designation=$_POST['designation'];
	$domain=$_POST['domain'];
	$company_address=$_POST['company_address'];
	$company_telephone=$_POST['company_telephone_no'];
	$reffered_by=$_POST['reffered_by'];
	$interest=$_POST['interest'];
	$participate=$_POST['participate'];
	$member_of_org=$_POST['member_of_org'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Register Member</title>
		<link rel="stylesheet" href="../css/main.css" type="text/css" media="all" />
		<script type="text/javascript">
			function isNumber(evt) {
				evt = (evt) ? evt : window.event;
				var charCode = (evt.which) ? evt.which : evt.keyCode;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					return false;
				}
				return true;
			}
		</script>
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "../templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container" style="height: 1500px;">
			<br/><br/>
			<div class="form_title">
				<h2>Member Registration Confirmation Form</h2><br/><hr>
			</div>
			
			<form name="confirm_member_form" id="add_member_form" action="member_added.php" onsubmit="return validate();" method="post">
			<?php
				
				echo "<table border='0' align='center'>
					<tr>
						<th colspan='4'><br/>
						<label>Personal Information</label><br/>
						</th>
					</tr>
					<tr>";
						if($library_id==""){}
						else{
						echo "<td>Library Id:</td>
						<td><input type='text' name='library_id' id='lib_id' placeholder=' Library Account Number' value='" .$library_id. "' onkeypress='return isNumber(event)'></td>";
						}
				echo "
					</tr>
					<tr>
						<td>Name:</td>
						<td><input type='text' name='first_name' placeholder=' First Name' value='".$first_name."'></td>
						<td><input type='text' name='middle_name' placeholder=' Middle Name' value='".$middle_name."'></td>
						<td><input type='text' name='last_name' placeholder=' Last Name' value='".$last_name."'></td>
					</tr>
					<tr>
						<td>Date Of Birth:</td>
						<td><input type='text' name='dob' placeholder=' Date Of Birth' value='".$dob."'></td>
					</tr>
					<tr>
						<td>Gender:</td>
						<td><input type='text' name='gender' placeholder=' Gender' value='".$gender."'></td>
					</tr>
					<tr>
						<td>Blood Group:</td>
						<td><input type='text' name='blood_group' placeholder=' Blood Group' value='".$blood_group."'></td>
					</tr>
					<tr>
						<td>Address:</td>
						<td><textarea name='address' rows='4' cols='24'>".$address."</textarea></td>
					</tr>
					<tr>
						<td></td>
						<td><input type='text' name='city' placeholder=' City' value='".$city."'></td>
					</tr>
					<tr>
						<td></td>
						<td><input type='text' name='state' placeholder=' State' value='".$state."'></td>
					</tr>
					<tr>
						<td></td>
						<td><input type='text' name='pincode' placeholder=' Pincode' value='".$pincode."' onkeypress='return isNumber(event)'></td>
					</tr>
					<tr>
						<td>Contact No:</td>
						<td><input type='text' name='mobile_no' placeholder=' Mobile' value='".$mobile."' onkeypress='return isNumber(event)'></td>
						<td><input type='text' name='landline_no' placeholder=' Landline With STD Code' value='".$landline."' onkeypress='return isNumber(event)'></td>
					</tr>
					<tr>
						<td>E-mail Id:</td>
						<td colspan='3'><input type='text' name='email_id' placeholder=' E-mail Id' style='width:250px' value='".$email_id."'></td>
					</tr>
					<tr>
						<th colspan='4'><br/>
							<label>Educational Information</label><br/>
						</th>
					</tr>
					<tr>
						<td>Course:</td>
						<td><input type='text' name='course' placeholder=' Course' value='".$course."'></td>
					</tr>
					<tr>
						<td>Branch:</td>
						<td><input type='text' name='branch' placeholder=' Branch' value='".$branch."'></td>
					</tr>
					<tr>
						<td>Study Year:</td>
						<td><input type='text' name='study_year' placeholder=' Year' value='".$study_year."'></td>
					</tr>
					<tr>
						<td>College:</td>
						<td><input type='text' name='college' placeholder=' college' value='".$college."'></td>
					</tr>
					<tr>
						<td>University:</td>
						<td><input type='text' name='university' placeholder=' University' value='".$university."'></td>
					</tr>
					
					<tr>
						<th colspan='4'><br/>
						<label>Professional Information</label><br/>
						</th>
					</tr>
					<tr>
						<td>Company:</td>
						<td><input type='text' name='company' placeholder=' Company Name' value='".$company."'></td>
					</tr>
					<tr>
						<td>Designation:</td>
						<td><input type='text' name='designation' placeholder=' Designation' value='".$designation."'></td>
					</tr>
					<tr>
						<td>Domain:</td>
						<td><input type='text' name='domain' placeholder=' Domain/Work Platform' value='".$domain."'></td>
					</tr>
					<tr>
						<td>Company Address:</td>
						<td colspan='3'><textarea name='company_address'>".$company_address."</textarea></td>
					</tr>
					<tr>
						<td>Telephone No:</td>
						<td><input type='text' name='company_telephone_no' placeholder=' Company Telephone' value='".$company_telephone."' onkeypress='return isNumber(event)'></td>
					</tr>
	
					<tr>
						<th colspan='4'><br/>
						<label>Professional Information</label><br/>
						</th>
					</tr>
					<tr>
						<td>Reffered By:</td>
						<td><input type='text' name='reffered_by' placeholder=' Reffered By' value='".$reffered_by."'></td>
					</tr>
					<tr>
						<td>Skills/Special Areas Of Interest:</td>
						<td colspan='3'><input type='text' name='interest' placeholder=' Enter Your Interest' style='width:250px' value='".$interest."'></td>
					</tr>
					<tr>
						<td>Do you wish to participate in project and actvities of VSM :</td>
						<td><input type='text' name='participate' placeholder=' Wish To Participate' value='".$participate."'></td>
					</tr>
					<tr>
						<td>If You Are a Member Of Any Other Social or<br>Professional Organisation,Please Mention It's Name:</td>
						<td colspan='3'><input type='text' name='member_of_org' placeholder=' Name Of Organisation' style='width:250px' value='".$member_of_org."'></td>
					</tr>
					<tr>
						<td colspan='4' style='text-align: center;'><br/><input type='submit' name='confirm' value='Confirm'><br></td>
					</tr>
				</table>";
			?>
			</form>
		</div>
		<!-- Content -->
		
		<!-- Footer_start -->
		<?php //include_once "../templates/footer_template.php"?>
		<!-- Footer_end -->
	</body>
</html>
