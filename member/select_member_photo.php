<?php
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Member Photo</title>
		<link rel="stylesheet" href="../css/main.css" type="text/css" media="all" />
		<script type="text/javascript">
			function isNumber(evt)
			{
				evt = (evt) ? evt : window.event;
				var charCode = (evt.which) ? evt.which : evt.keyCode;
				if (charCode > 31 && (charCode < 48 || charCode > 57))
				{
					return false;
				}
				return true;
			}
		</script>
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "../templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container" style="height: 1500px;">
			<br/><br/>
			<div class="form_title">
				<h2>Member Photo</h2><br/><hr>
			</div>
			
			<form name="member_photo_form" id="member_photo_form" action="upload_member_photo.php" method="post" enctype="multipart/form-data">
				<table border='0' align='center'>
					<tr>
						<td>Member ID</td>
						<td><input type="text" name="member_id" placeholder=" Member Id" value="<?php if(isset($_GET['member_id'])){echo $_GET['member_id'];} ?>" onkeypress="return isNumber(event)"/></td>
					</tr>
					<tr>
						<td><label for="photo">Member Photo:</label></td>
						<td><input type="file" name="photo" id="photo"></td>
					</tr>
					<tr>
						<td colspan="2" align="center"><br/><input type="submit" name="submit" value="Upload Photo"></td>
					</tr>
					<tr>
						<td><font size="2">Maximum size allowed: 1 Mb</font></td>
					</tr>
					<tr>
						<td><font size="2">Supported formats: .gif, .jpg, .jpeg, .png</font></td>
					</tr>
				</table>
			</form>
			<br><br>
			<?php
				if(isset($_GET['success']))
				{
					if($_GET['success']==1)
					{
						echo "<div style='color:green;margin-left:10px'>Member photo upload successful. Thank you !</div>";
					}
					if($_GET['success']==2)
					{
						echo "<div style='color:red;margin-left:10px'>Member photo upload unsuccessful. Invalid member photo. Please check photo size or extension.</div>";
					}
					if($_GET['success']==3)
					{
						echo "<div style='color:red;margin-left:10px'>Member photo upload unsuccessful. Invalid member ID.</div>";
					}
				}
			?>
			
		</div>
		<!-- Content -->
		
		<!-- Footer_start -->
		<?php //include_once "../templates/footer_template.php"?>
		<!-- Footer_end -->
	</body>
</html>