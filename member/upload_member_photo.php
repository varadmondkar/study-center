<?php
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
	
	$member_id = $_POST['member_id'];
	
	include_once '../db_config/db_config.php';
				
	$select = "SELECT * FROM sc_member WHERE member_id='$member_id'";
	$result = mysql_query($select) or die("ERROR 1 : ".mysql_error());
	$member_count = mysql_num_rows($result);
	
	if($member_count == 1)
	{
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["photo"]["name"]);
		$extension = end($temp);
		$member_photo = $member_id.".".$extension;//new name of photo .....renaming it by member id.extension

		if ((($_FILES["photo"]["type"] == "image/gif")
		|| ($_FILES["photo"]["type"] == "image/jpeg")
		|| ($_FILES["photo"]["type"] == "image/jpg")
		|| ($_FILES["photo"]["type"] == "image/pjpeg")
		|| ($_FILES["photo"]["type"] == "image/x-png")
		|| ($_FILES["photo"]["type"] == "image/png"))
		&& ($_FILES["photo"]["size"] < 1048576)// 1 Mb = 1048576 bits
		&& in_array($extension, $allowedExts))
		{
			if ($_FILES["photo"]["error"] > 0)
			{
				echo "Error 2: " . $_FILES["photo"]["error"] . "<br>";
			}
			else 
			{
				if (file_exists("member_photos/" . $_FILES["photo"]["name"]))
				{
					echo "Member photo <b>".$_FILES["photo"]["name"] . " </b>already exists.Please chnage the name of photo and try again ";
				}
				else
				{
					$update = "UPDATE sc_member SET member_photo='$member_photo' WHERE member_id='$member_id'";
					$result = mysql_query($update) or die("ERROR 3 : ".mysql_error());
						
					move_uploaded_file($_FILES["photo"]["tmp_name"],"member_photos/" . $member_photo);
					header('Location: select_member_photo.php?success=1');//upload successful 
				}
			}
		}
		else
		{
			header('Location: select_member_photo.php?success=2');//invalid photo
		}
	} 
	else
	{
		header('Location: select_member_photo.php?success=3');//invalid member id
	}
?>