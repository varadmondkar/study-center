<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Lock Member</title>
		<link rel="stylesheet" href="css/main.css" type="text/css" media="all" />
		<script type="text/javascript">
			function show_member(str)
			{
			if (str.length==0)
			  { 
			  document.getElementById("txtHint").innerHTML="";
			  return;
			  }
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			    {
			    document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
			    }
			  }
			xmlhttp.open("GET","member/lock_check.php?q="+str,true);
			xmlhttp.send();
			}

			function isNumber(evt) {
				evt = (evt) ? evt : window.event;
				var charCode = (evt.which) ? evt.which : evt.keyCode;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					return false;
				}
				return true;
			}
		</script>
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "templates/header_template.php"; ?>
		<!-- header_end -->

		<!-- Content_starts -->
		<div id="container" style="height: 800px;">

			<div class="form_title">
				<h2>Member Lock Form</h2><br/><hr>
			</div>
			
			<center>
			<form name="lock_member_form" id="lock_member_form" method="post" action="lock_member.php">
				<table border="0">
					<tr>
						<td>Member ID</td>
						<td><input type="text" name="member_id" placeholder=" Member Id" onkeyup="show_member(this.value)" onClick="show_member(this.value)" value="<?php if(isset($_GET['member_id'])){echo $_GET['member_id'];} ?>" onkeypress="return isNumber(event)"/></td>
					</tr>
				</table>
			</form>
			<?php
				if (isset($_GET['member_id'])&&isset($_GET['lock']))
				{
					$lock=$_GET['lock'];
					$member_id=$_GET['member_id'];

					include 'db_config/db_config.php';
					$select = "SELECT * FROM sc_member WHERE member_id='$member_id'";
					$result = mysql_query($select) or die("ERROR 1 : ".mysql_error());
					$member_count = mysql_num_rows($result);
					while($rows=mysql_fetch_array($result))
					{
						$member_name = $rows['member_name'];
					}

					if($lock==1)
					{
						echo "<br>Member <a href='view_member_details.php?member_id=".$member_id."'>".$member_name."</a> is currently Locked.";
					}
					else if($lock==0)
					{
						echo "<br>Member <a href='view_member_details.php?member_id=".$member_id."'>".$member_name."</a> is currently Unlocked.";
					}
				}
			?>

			<span id="txtHint">

			</span>
			
			</center>
		</div>
		<!-- Content_end -->
		
		<!-- Footer_start -->
		<?php //include_once "templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>