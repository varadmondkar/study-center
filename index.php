<?php 
	session_start(); 
	if(isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/admin/');
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Admin Login</title>
		<link rel="stylesheet" href="css/main.css" type="text/css" media="all" />
<style>
body {
	background: -webkit-linear-gradient(#F2F2F2, #F2F2F2, #F2F2F2); /* For Safari */
	background: -o-linear-gradient(#F2F2F2, #F2F2F2, #F2F2F2); /* For Opera 11.1 to 12.0 */
	background: -moz-linear-gradient(#F2F2F2, #F2F2F2, #F2F2F2); /* For Firefox 3.6 to 15 */
	background: linear-gradient(#F2F2F2, #F2F2F2, #F2F2F2); /* Standard syntax (must be last) */
}
input[type=text], input[type=password] {
	font-family: 'Verdana', Tahoma, Lucida Grande, sans-serif;
	font-size: 14px;
	margin: 5px;
	padding: 0 10px;
	width: 200px;
	height: 34px;
	color: #404040;
	background: white;
	border: 1px solid;
	border-color: #c4c4c4 #d1d1d1 #d4d4d4;
	border-radius: 2px;
	outline: 5px solid #eff4f7;
	-moz-outline-radius: 3px;
	-webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.12);
	box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.12);
}
input[type=text]:focus, input[type=password]:focus {
	border-color: #848484;/* #7dc9e2; */
	outline-color: #D8D8D8;/* #dceefc; */
	outline-offset: 0;
}

input[type=submit] {
	padding: 2px 130px;
	line-height: 14px;
	height: 29px;
	font-size: 14px;
	font-weight: bold;
	color: #555;
	text-shadow: 0 1px #e3f1f1;
	background: #D3D9D4;
	border: 1px solid;
	border-color: #b4ccce #b3c0c8 #9eb9c2;
	border-radius: 6px;
	outline: 0;
	-webkit-box-sizing: content-box;
	-moz-box-sizing: content-box;
	box-sizing: content-box;
	background-image: -webkit-linear-gradient(top, #FAFAFA, #D3D9D4);
	background-image: -moz-linear-gradient(top, #FAFAFA, #D3D9D4);
	background-image: -o-linear-gradient(top, #FAFAFA, #D3D9D4);
	background-image: linear-gradient(to bottom, #FAFAFA, #D3D9D4);
	-webkit-box-shadow: inset 0 1px white, 0 1px 2px rgba(0, 0, 0, 0.15);
	box-shadow: inset 0 1px white, 0 1px 2px rgba(0, 0, 0, 0.15);
}
input[type=submit]:active {
	background: #D3D9D4;
	border-color: #9eb9c2 #b3c0c8 #b4ccce;
	-webkit-box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.2);
	box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.2);
}

.lt-ie9 input[type=text], .lt-ie9 input[type=password] {
	line-height: 34px;
}
</style>
	</head>

	<body>
		<!-- header_start -->
		<?php //include_once "templates/login_header_template.php"; ?>
		<!-- header_end -->

		<!-- <div id="container" style="height: 700px;"> -->
			<br><br><br>
			<h1 align='center' style='color:#555'>Welcome to Vivekanand Seva Mandal</h1>
			<br><br><br><br><br><br><br><br><br>

			<div class="login">
				<h1>Administrator/Guest Login</h1>
				<form name="admin_login_form" id="admin_login_form" action="admin/login_confirm.php" method="post">
					<p><input type="text" name="username" value="" placeholder="Username or Mobile Number"></p>
					<p><input type="password" name="password" value="" placeholder="Password"></p>
					<!-- 	<p class="remember_me">
						<label><input type="checkbox" name="remember_me" id="remember_me">Remember me on this computer</label>
					</p> -->
					<p class="submit"><input type="submit" name="commit" value="Login"></p>
				</form>
				<br>
				<a href="http://localhost/study_center/admin/user/forgot_password.php" style="text-decoration:none">Forgot your password?</a>
			</div>
			
			<?php
				if (isset($_GET['login']))
				{
					$login=$_GET['login'];
					
					if($login==0)
					{
						echo "<center><br><font color='red' size='4'>Sorry, Invalid login details.</font></center>";
					}
				}
			?>
			
		<!-- </div> -->
			<br><br><br><br><br><br>
		<!-- Footer_start -->
		<?php //include_once "templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>