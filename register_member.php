<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
	include 'db_config/db_config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Register Member</title>
		<link rel="stylesheet" href="css/main.css" type="text/css" media="all" />
		<script type="text/javascript" src="javascript/register_member.js"></script>
		<script type="text/javascript">
			function showAccountNoField(checkbox)
			{
				if(checkbox.checked)
				{
					document.getElementById('lib_id').style.visibility="visible";
					document.add_member_form.library_id.focus();
				}
				else
				{
					document.getElementById('lib_id').style.visibility="hidden";
				}
			}
			function isNumber(evt) {
				evt = (evt) ? evt : window.event;
				var charCode = (evt.which) ? evt.which : evt.keyCode;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					return false;
				}
				return true;
			}
		</script>
		<style type="text/css">
		#lastadded {
			text-align: center;
		}
		/*::-webkit-input-placeholder {
			color: red;
		}
		:-moz-placeholder {
			color: red;
		}
		::-moz-placeholder {
			color: red;
		}
		:-ms-input-placeholder {
			color: red;
		}*/
		</style>
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container" style="height: auto;">

			<div class="form_title">
				<h2>Member Registration Form</h2><br/><hr>
			</div>

			<div id="lastadded">
				<?php
				$query1 = mysql_query("SELECT member_id, member_name FROM `sc_member` ORDER BY member_id DESC LIMIT 1");
				$data = mysql_fetch_array($query1);
				echo "<b>Last Member Added</b>";
				echo "<br/><h5>";
				echo $data['member_id'];
				echo " - ";
				echo $data['member_name'];
				echo "</h5>";
				?>	
			</div>
			
			<form name="add_member_form" id="add_member_form" action="member/confirm_member.php" onsubmit="return validate();" method="post">
				<table border="0" align="center">
					<tr>
						<th colspan="4">
						<label>Personal Information</label><br/>
						</th>
					</tr>
					<tr>
						<td>Are you member of library? &nbsp;<input type="checkbox" name="member_check" id="check_if_member" onclick="showAccountNoField(this)"><br/></td>
						<td><input type="text" name="library_id" id="lib_id" placeholder=" Library ID " style="visibility:hidden;" onkeypress="return isNumber(event)"></td>
					</tr>
					<tr>
						<td>Name:</td>
						<td><input type="text" name="first_name" placeholder=" First name"></td>
						<td><input type="text" name="middle_name" placeholder=" Middle name"></td>
						<td><input type="text" name="last_name" placeholder=" Last name"></td>
					</tr>
					<tr>
						<td>Date Of Birth:</td>
						<td>
							<select name="day" style="width:150px;">
								<option value=""> Day</option>
								<option value="01">1</option>
								<option value="02">2</option>
								<option value="03">3</option>
								<option value="04">4</option>
								<option value="05">5</option>
								<option value="06">6</option>
								<option value="07">7</option>
								<option value="08">8</option>
								<option value="09">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="24">24</option>
								<option value="25">25</option>
								<option value="26">26</option>
								<option value="27">27</option>
								<option value="28">28</option>
								<option value="29">29</option>
								<option value="30">30</option>
								<option value="31">31</option>
							</select>
						</td>
						<td>
							<select name="month" style="width:150px;">
								<option value=""> Month</option>
								<option value="01">January</option>
								<option value="02">February</option>
								<option value="03">March</option>
								<option value="04">April</option>
								<option value="05">May</option>
								<option value="06">June</option>
								<option value="07">July</option>
								<option value="08">August</option>
								<option value="09">September</option>
								<option value="10">October</option>
								<option value="11">November</option>
								<option value="12">December</option>
							</select>
						</td>
						<td>
							<select name="year" style="width:150px;">
								<option value=""> Year</option>
								<option value="2008">2008</option>
								<option value="2007">2007</option>
								<option value="2006">2006</option>
								<option value="2005">2005</option>
								<option value="2004">2004</option>
								<option value="2003">2003</option>
								<option value="2002">2002</option>
								<option value="2001">2001</option>
								<option value="2000">2000</option>
								<option value="1999">1999</option>
								<option value="1998">1998</option>
								<option value="1997">1997</option>
								<option value="1996">1996</option>
								<option value="1995">1995</option>
								<option value="1994">1994</option>
								<option value="1993">1993</option>
								<option value="1992">1992</option>
								<option value="1991">1991</option>
								<option value="1990">1990</option>
								<option value="1989">1989</option>
								<option value="1988">1988</option>
								<option value="1987">1987</option>
								<option value="1986">1986</option>
								<option value="1985">1985</option>
								<option value="1984">1984</option>
								<option value="1983">1983</option>
								<option value="1982">1982</option>
								<option value="1981">1981</option>
								<option value="1980">1980</option>
								<option value="1979">1979</option>
								<option value="1978">1978</option>
								<option value="1977">1977</option>
								<option value="1976">1976</option>
								<option value="1975">1975</option>
								<option value="1974">1974</option>
								<option value="1973">1973</option>
								<option value="1972">1972</option>
								<option value="1971">1971</option>
								<option value="1970">1970</option>
								<option value="1969">1969</option>
								<option value="1968">1968</option>
								<option value="1967">1967</option>
								<option value="1966">1966</option>
								<option value="1965">1965</option>
								<option value="1964">1964</option>
								<option value="1963">1963</option>
								<option value="1962">1962</option>
								<option value="1961">1961</option>
								<option value="1960">1960</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Gender:</td>
						<td>
							<select name="gender" style="width:150px;">
								<option value=""> Gender</option>
								<option value="Male">Male</option>
								<option value="Female">Female</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Blood Group:</td>
						<td>
							<select name="blood_group" style="width:150px;">
								<option value=""> Blood group</option>
								<option value="O+">O+</option>
								<option value="A+">A+</option>
								<option value="B+">B+</option>
								<option value="AB+">AB+</option>
								<option value="O-">O-</option>
								<option value="A-">A-</option>
								<option value="B-">B-</option>
								<option value="AB-">AB-</option>
								<option value="Unknown">Unknown</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Address:</td>
						<td><input type="text" name="add1" placeholder=" House No/Floor No"></td>
						<td colspan="2"><input type="text" name="add2" placeholder=" Building Name" style="width:300px;"></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="text" name="add3" placeholder=" Street Name/Road"></td>
						<td colspan="2"><input type="text" name="add4" placeholder=" Landmark/Area" style="width:300px;"></td>
					</tr>
					<tr>
						<td></td>
						<td>
							<select name="city" style="width:150px;">
								<option value=""> City</option>
								<option value="Dombivli">Dombivli</option>
								<option value="Kalyan">Kalyan</option>
								<option value="Thane">Thane</option>
								<option value="Other">Other</option>
							</select>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<select name="state" style="width:150px;">
								<option value=""> State</option>
								<option value="Maharashtra">Maharashtra</option>
								<option value="Goa">Goa</option>
								<option value="Other">Other</option>
							</select>
						</td>
					</tr>
					<tr>
						<td></td>
						<td><input type="text" name="pincode" placeholder=" Pincode" onkeypress="return isNumber(event)"></td>
					</tr>
					<tr>
						<td>Contact no:</td>
						<td><input type="text" name="mobile_no" placeholder=" 10 Digit Mobile Number" onkeypress="return isNumber(event)"></td>
						<td><input type="text" name="landline_no" placeholder=" Landline with STD code eg 02512442323" onkeypress="return isNumber(event)"></td>
					</tr>
					<tr>
						<td>E-mail id:</td>
						<td colspan="3"><input type="text" name="email_id" placeholder=" E-mail id" style="width:250px"></td>
					</tr>
		
					<tr>
						<th colspan="4"><br/>
						<label>Educational Information</label><br/>
						</th>
					</tr>
					<tr>
						<td>Course:</td>
						<td>
							<select name="course" style="width:150px;">
								<option value=""> Course</option>
								<option value="Engineering Degree">Engineering Degree</option>
								<option value="Enggineering Diploma">Engineering Diploma</option>
								<option value="Upsc/Mpsc">UPSC/MPSC</option>
								<option value="CA">CA</option>
								<option value="BSC">BSC</option>
								<option value="Medical">Medical</option>
								<option value="HSC">HSC</option>
								<option value="SSC">SSC</option>
								<option value="BCOM/MCOM">BCOM/MCOM</option>
								<option value="Other">Other</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Branch:</td>
						<td>
							<?php 
							include 'db_config/db_config.php';
							$query = "SELECT * FROM sc_branch";
							$result = mysql_query($query);
							?>
							<select name="branch">
								<option value=''> Branch</option>
								<?php
								while ($branch = mysql_fetch_array($result))
								{
									extract($branch);
									echo "<option value='".$branch_name."'>".$branch_name."</option>";
								}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Year:</td>
						<td>
							<select class="list_padding" name="study_year" style="width:150px;">
								<option value=""> Year</option>
								<option value="None"> None</option>
								<option value="First Year">First Year</option>
								<option value="Second Year">Second Year</option>
								<option value="Third Year">Third Year</option>
								<option value="Final Year">Final Year</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>College:</td>
						<td>
							<?php 
							include 'db_config/db_config.php';
							$query = "SELECT * FROM sc_college";
							$result = mysql_query($query);
							?>
							<select name="college">
								<option value=''> College</option>
								<?php
								while ($college = mysql_fetch_array($result))
								{
									extract($college);
									echo "<option value='".$college_name."'>".$college_name."</option>";
								}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>University:</td>
						<td>
							<?php 
							include 'db_config/db_config.php';
							$query = "SELECT * FROM sc_university";
							$result = mysql_query($query);
							?>
							<select name="university">
								<option value=''> University</option>
								<?php
								while ($university = mysql_fetch_array($result))
								{
									extract($university);
									echo "<option value='".$university_name."'>".$university_name."</option>";
								}
								?>
							</select>
						</td>
					</tr>
					
					<tr>
						<th colspan="4"><br/>
						<label>Professional Information</label><br/>
						</th>
					</tr>
					<tr>
						<td>Company:</td>
						<td><input type="text" name="company" placeholder=" Company Name"></td>
					</tr>
					<tr>
						<td>Designation:</td>
						<td><input type="text" name="designation" placeholder=" Designation"></td>
					</tr>
					<tr>
						<td>Domain:</td>
						<td><input type="text" name="domain" placeholder=" Domain/Work Platform"></td>
					</tr>
					<tr>
						<td>Company Address:</td>
						<td colspan="3"><textarea name="company_address" cols="24"></textarea></td>
					</tr>
					<tr>
						<td>Telephone no:</td>
						<td><input type="text" name="company_telephone_no" placeholder=" Company telephone" onkeypress="return isNumber(event)"></td>
					</tr>
	
					<tr>
						<th colspan="4"><br/>
						<label>Other Information</label><br/>
						</th>
					</tr>
					<tr>
						<td>Reffered By:</td>
						<td>
							<select name="reffered_by" style="width:150px;">
								<option value=""> Select option</option>
								<option value="Friend">Friend</option>
								<option value="Hoarding">Hoarding</option>
								<option value="Mandal Website">Mandal Website</option>
								<option value="Other">Other</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Skills/Special areas of interest:</td>
						<td colspan="3"><input type="text" name="interest" placeholder=" Enter your interest" style="width:250px"></td>
					</tr>
					<tr>
						<td>Do you wish to participate in project and actvities of VSM :</td>
						<td>
							<select class="list_padding" name="participate" style="width:150px;">
								<option value="">Select option</option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>if you are a member of any other social or<br>professional organisation,please mention it's name:</td>
						<td colspan="3"><input type="text" name="member_of_org" placeholder=" Name of organisation" style="width:250px"></td>
					</tr>
					<tr>
						<td colspan="4" style="text-align: center;"><br/><input type="submit" value="Register"><br></td>
					</tr>
				</table>
			</form>
		</div>
		<!-- Content -->
		
		<!-- Footer_start -->
		<?php //include_once "templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>