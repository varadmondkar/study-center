<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
?>
<!DOCTYPE>
<html>
	<head>
		<title>Member Fee</title>
		<link rel="stylesheet" href="css/main.css" type="text/css" media="all" />
		<link rel="stylesheet" type="text/css" media="all" href="css/jsDatePick_ltr.min.css" />

		<script type="text/javascript" src="javascript/member_fee.js"></script>
		<script type="text/javascript" src="javascript/jquery.1.4.2.js"></script>
		<script type="text/javascript" src="javascript/jsDatePick.jquery.min.1.3.js"></script>
		<script type="text/javascript">
			function showHint(str)
			{
			if (str.length==0)
			  { 
			  document.getElementById("txtHint").innerHTML="";
			  return;
			  }
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
				document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
				}
			  }
			xmlhttp.open("GET","fee/fee_check.php?id="+str,true);
			xmlhttp.send();
			}

			function isNumber(evt) {
				evt = (evt) ? evt : window.event;
				var charCode = (evt.which) ? evt.which : evt.keyCode;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					return false;
				}
				return true;
			}
		</script>		
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container">

			<div class="form_title">
				<h2>Member Fee Form</h2><br/><hr>
			</div>
			
			<form name="member_fee_form" id="member_fee_form" action="fee/fee_check.php" method="POST" onsubmit="return validate();">
			<table border="0" align="center">
				<tr>
					<td><br/>Member ID:</td>
					<td><br/><input type="text" name="member_id"  placeholder=" Member Id" onkeyup="showHint(this.value)" onClick="showHint(this.value)" value="<?php if(isset($_GET['member_id'])){echo $_GET['member_id'];} ?>"  onkeypress="return isNumber(event)"/></td>
				</tr>
			</table>
			
			</form>
			<?php
			if(isset($_REQUEST['no_receipt_booklet']))
				echo "<div style='margin-left:20px;color:red'>Fees not paid for member id ".$_GET['id']." due to incomplete filled form. Please fill complete form to pay fees.</div>";
			?>
			<span id="txtHint"></span>

		</div>
		
		<!-- Footer_start -->
		<?php //include_once "templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>