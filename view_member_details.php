<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
	include 'db_config/db_config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>View Member</title>
		<link rel="stylesheet" href="http://localhost/study_center/css/main.css" type="text/css" media="all" />
		<script type="text/javascript">
			function isNumber(evt) {
				evt = (evt) ? evt : window.event;
				var charCode = (evt.which) ? evt.which : evt.keyCode;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					return false;
				}
				return true;
			}
		</script>
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container" style="height: 1500px;">
		<?php
		if(isset($_POST['update_member_details']))
		{
			$member_id = $_REQUEST['member_id'];
			$name = $_POST['name'];
			$dob = $_POST['dob'];
			$gender = $_POST['gender'];
			$blood_group = $_POST['blood_group'];
			$address = $_POST['address'];
			$city = $_POST['city'];
			$state = $_POST['state'];
			$mobile = $_POST['mobile_no'];
			$landline = $_POST['landline_no'];
			$email_id = $_POST['email_id'];
			$course = $_POST['course'];
			$branch = $_POST['branch'];
			$study_year = $_POST['study_year'];
			$college = $_POST['college'];
			$university = $_POST['university'];
			$company = $_POST['company'];
			$designation = $_POST['designation'];
			$domain = $_POST['domain'];
			$company_address = $_POST['company_address'];
			$company_telephone = $_POST['company_telephone_no'];
			$reffered_by = $_POST['reffered_by'];
			$interest = $_POST['interest'];
			$participate = $_POST['participate'];
			$member_of_org = $_POST['member_of_org'];
			$library_id = $_POST['library_id'];
		
		$update_details = "UPDATE sc_member SET member_name = '$name', member_dob = '$dob', member_gender = '$gender', member_blood_group = '$blood_group', 
			member_address = '$address', member_city = '$city', member_state = '$state', member_mobile_number = '$mobile', member_residence_number = '$landline',
			member_email_id = '$email_id', member_course = '$course', member_branch_name = '$branch', member_year = '$study_year', member_college_name = '$college', 
			member_university_name = '$university', member_company_name = '$company', member_company_designation = '$designation', member_company_domain = '$domain', 
			member_company_address = '$company_address', member_company_telephone = '$company_telephone', member_reffered_by = '$reffered_by', 
			member_interest = '$interest', member_wish_to_participate_in_vsm = '$participate', member_of_other_org = '$member_of_org' WHERE member_id = '$member_id';";
		$update_member_result= mysql_query($update_details) or die("ERROR 1 : ".mysql_error());
		if($update_member_result == TRUE)
			header('Location: http://localhost/study_center/view_member_details.php?member_id='.$member_id.'&success=1');
		else
			header('Location: http://localhost/study_center/view_member_details.php?member_id='.$member_id.'&success=0');
		}
		?>

		<?php
	
			if (isset($_GET['member_id']))
			{
				$member_id = $_GET['member_id'];
				
				/********************************* Unsetting the fee flag *****************************************/
				$fee_due_date_query = "SELECT fee_due_date FROM sc_fee WHERE fee_member_id='$member_id'";
				$fee_due_date_result = mysql_query($fee_due_date_query) or die("ERROR 1 : ".mysql_error());
				$member_fee_row_count = mysql_num_rows($fee_due_date_result);
				while($rows=mysql_fetch_array($fee_due_date_result))
					extract($rows);

				// If new member enters than he/she does not have any entry in sc_fee and sc_fee_history hence no due_date so check member fee row count
				if($member_fee_row_count != 0)
				{
					$today_date=date("Y-m-d");
					
					if($today_date >= $fee_due_date)
					{
						$update_fee_flag_query = "UPDATE sc_member_flag SET fee_flag=0 WHERE member_flag_id='$member_id'";
						$update_fee_flag = mysql_query($update_fee_flag_query) or die("ERROR 2 : ".mysql_error());
						$update_member_status_query = "UPDATE sc_member SET member_status=0 WHERE member_id='$member_id'";
						$update_member_status_result = mysql_query($update_member_status_query) or die("ERROR 3 : ".mysql_error());
					}
				}
				/******************************* End of Unsetting the fee flag *****************************************/

				$select = "SELECT * FROM sc_member WHERE member_id = '$member_id'";
				$result = mysql_query($select) or die("ERROR1 : ".mysql_error());
				$count = mysql_num_rows($result) or die("ERROR2 : ".mysql_error());

				while($rows = mysql_fetch_array($result))
				{
					extract($rows);
				}
			?>

			<div class="form_title">
				<h2>Member Details (Member is currently 
			<?php
			if($member_status==0)
				$member_status = "<font style='color:red'>Inactive</font>";
			else if($member_status==1)
				$member_status = "<font style='color:green'>Active</font>";
			else if($member_status==2)
				$member_status = "<font style='color:blue'>Locked</font>";
			echo $member_status;
			?>)</h2><br/><hr>
			</div>
			<?php
			if(isset($_GET['success']) AND $_GET['success'] == 1)
				echo "<div style='text-align:center;color:green'>Member details successfully updated.</div>";
			else if(isset($_GET['success']) AND $_GET['success'] == 0)
				echo "<div style='text-align:center;color:red'>Failed to updated Member details.</div>";
			?>
			<form name='add_member_form' id='add_member_form' action='view_member_details.php' method='post'>
				<table border='0' width='100%' align='center'>
				<tr>
					<th colspan='4'><br/>
					<label>Personal Information</label><br/>
					<input type='hidden' name='member_id' value='<?php echo $member_id ?>'>
					</th>
				</tr>
				<tr>
					<td>Library Id:</td>
					<td><input type='text' name='library_id' id='lib_id' value='<?php echo $member_library_id; ?>' readonly></td>
				</tr>
				<tr>
					<td>Name:</td>
					<td><input type='text' name='name' value='<?php echo $member_name; ?>'></td>
				</tr>
				<tr>
					<td>Date Of Birth:</td>
					<td><input type='text' name='dob' value='<?php echo $member_dob; ?>'></td>
				</tr>
				<tr>
					<td>Gender:</td>
					<td><input type='text' name='gender' value='<?php echo $member_gender; ?>'></td>
				</tr>
				<tr>
					<td>Blood Group:</td>
					<td><input type='text' name='blood_group' value='<?php echo $member_blood_group; ?>'></td>
				</tr>
				<tr>
					<td>Address:</td>
					<td><textarea name='address' style='width:212px'><?php echo $member_address; ?></textarea></td>
				</tr>
				<tr>
					<td>City:</td>
					<td><input type='text' name='city' value='<?php echo $member_city; ?>'></td>
				</tr>
				<tr>
					<td>State:</td>
					<td><input type='text' name='state' value='<?php echo $member_state; ?>'></td>
				</tr>
				<tr>
					<td>Contact No(Mobile):</td>
					<td><input type='text' name='mobile_no' value='<?php echo $member_mobile_number; ?>' onkeypress="return isNumber(event)"></td>
				</tr>
				<tr>
					<td>Contact No(Landline):</td>
					<td><input type="text" name='landline_no' value='<?php echo $member_residence_number; ?>' onkeypress="return isNumber(event)"></td>
				</tr>
				<tr>
					<td>E-mail Id:</td>
					<td colspan='3'><input type='text' name='email_id' style='width:250px' value='<?php echo $member_email_id; ?>'></td>
				</tr>
				<tr>
					<th colspan='4'><br/>
						<label>Educational Information</label><br/>
					</th>
				</tr>
				<tr>
					<td>Course:</td>
					<td><input type='text' name='course' value='<?php echo $member_course; ?>'></td>
				</tr>
				<tr>
					<td>Branch:</td>
					<td><input type='text' name='branch' value='<?php echo $member_branch_name; ?>'></td>
				</tr>
				<tr>
					<td>Study Year:</td>
					<td><input type='text' name='study_year' value='<?php echo $member_year; ?>'></td>
				</tr>
				<tr>
					<td>College:</td>
					<td><input type='text' name='college' value='<?php echo $member_college_name; ?>'></td>
				</tr>
				<tr>
					<td>University:</td>
					<td><input type='text' name='university' value='<?php echo $member_university_name; ?>'></td>
				</tr>
				
				<tr>
					<th colspan='4'><br/>
					<label>Professional Information</label><br/>
					</th>
				</tr>
				<tr>
					<td>Company:</td>
					<td><input type='text' name='company' value='<?php echo $member_company_name; ?>'></td>
				</tr>
				<tr>
					<td>Designation:</td>
					<td><input type='text' name='designation' value='<?php echo $member_company_designation; ?>'></td>
				</tr>
				<tr>
					<td>Domain:</td>
					<td><input type='text' name='domain' value='<?php echo $member_company_domain; ?>'></td>
				</tr>
				<tr>
					<td>Company Address:</td>
					<td colspan='3'><textarea name='company_address' style='width:212px'><?php echo $member_company_address; ?></textarea></td>
				</tr>
				<tr>
					<td>Telephone No:</td>
					<td><input type='text' name='company_telephone_no' value='<?php echo $member_company_telephone; ?>' onkeypress="return isNumber(event)"></td>
				</tr>

				<tr>
					<th colspan='4'><br/>
					<label>Professional Information</label><br/>
					</th>
				</tr>
				<tr>
					<td>Reffered By:</td>
					<td><input type='text' name='reffered_by' value='<?php echo $member_reffered_by; ?>'></td>
				</tr>
				<tr>
					<td>Skills/Special Areas Of Interest:</td>
					<td colspan='3'><input type='text' name='interest' style='width:200px' value='<?php echo $member_interest; ?>'></td>
				</tr>
				<tr>
					<td>Do You Wish To Participate In Project And Actvities Of VSM :</td>
					<td><input type='text' name='participate' value='<?php echo $member_wish_to_participate_in_vsm; ?>'></td>
				</tr>
				<tr>
					<td>If You Are a Member Of Any Other Social or<br>Professional Organisation,Please Mention It's Name:</td>
					<td colspan='3'><input type='text' name='member_of_org'  style='width:200px' value='<?php echo $member_of_other_org; ?>'></td>
				</tr>
				<tr>
					<td colspan='4' style='text-align: center;'><br/><input type='submit' name='update_member_details' style='width:200px' value='Update member details'><br></td>
				</tr>
			</table>
			</form>
		<?php
			}
		?>
			
		</div>
		<!-- Content -->
		
		<!-- Footer_start -->
		<?php //include_once "templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>