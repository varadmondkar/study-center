<div id="header">
	<!-- header_content_start -->
	<div id="header_content">

		<!-- header_top_start -->
		<div id="header_top">
			<div class="logo"><img src="http://localhost/study_center/images/greenbg.jpg" alt="logo" width="100px" height="56px"></div>
			<div class="title"><h2><br>Vivekanand Seva Mandal - Study Center</h2></div>
		</div>
		<!-- header_top_end -->

		<!-- header_bottom_start -->
		<?php
		if(isset($_SESSION['admin_id']))
		{
		?>
			<div id='header_bottom'>
			<center>
				<ul align='center' id='menu'>
					<li><a>Member</a>
						<ul class='sub-menu'>
							<li><a href='http://localhost/study_center/register_member.php'>Add Member</a></li>
							<li><a href='http://localhost/study_center/lock_member.php'>Lock Member</a></li>
						</ul>
					</li>
					<li><a>Fee</a>
						<ul class='sub-menu'>
							<li><a href='http://localhost/study_center/member_fee.php'>Pay Fee</a></li>
							<li><a href='http://localhost/study_center/fee/update_fee.php'>Update Fee</a></li>
						</ul>
					</li>
					<li><a href='http://localhost/study_center/member_entry.php'>Member Entry</a></li>
					<li><a href='http://localhost/study_center/admin/index.php'>Admin Module</a></li>
<!-- 					<li><a href=''>Search</a>
						<ul class='sub-menu'>
							<li><input type='text' placeholder='Member id / Member name' /></li>
						</ul>
					</li>-->
					<li><a href="http://localhost/study_center/admin/logout.php">Logout</a></li>
				</ul>
			</center>
			</div>
		<?php
		}
		?>
		<!-- header_bottom_end -->
		
	</div>
	<!-- header_content_end -->
</div>