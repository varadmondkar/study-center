<div id="header">
	<!-- header_content_start -->
	<div id="header_content">

		<!-- header_top_start -->
		<div id="header_top">
			<div class="logo"><img src="http://localhost/study_center/images/swami.jpg" alt="logo" width="100px" height="80px"></div>
			<div class="title"><h2><br>Vivekanand Seva Mandal - Study Center</h2></div>
		</div>
		<!-- header_top_end -->

		<!-- header_bottom_start -->
		<?php
		if(isset($_SESSION['admin_id']))
		{
		?>
			<div id='header_bottom'>
			<center>
				<ul align='center' id='menu'>
					<li><a>Member</a>
						<ul class='sub-menu'>
							<li><a href='http://localhost/study_center/register_member.php'>Add Member</a></li>
							<li><a href='http://localhost/study_center/lock_member.php'>Lock Member</a></li>
						</ul>
					</li>
					<li><a href='http://localhost/study_center/member_fee.php'>Fee</a></li>
					<li><a href='http://localhost/study_center/member_entry.php'>Member Entry</a></li>
					<li><a href='http://localhost/study_center/admin/index.php'>Admin Module</a></li>
<!-- 					<li><a href=''>Search</a>
						<ul class='sub-menu'>
							<li><input type='text' placeholder='Member id / Member name' /></li>
						</ul>
					</li>-->
					<li><a href="http://localhost/study_center/admin/logout.php">Logout</a></li>
				</ul>
			</center>
			</div>
		<?php
		}
		?>
		<!-- header_bottom_end -->
		
	</div>
	<!-- header_content_end -->
</div>

<div id="upper-container">
<center>
<form>
<input type="text" name="member" placeholder="Enter Member id/Member name here to search" onkeyup="search_member(this.value)" style="width:300px;">
</form>
</center><br/>

<span id="Hint">
</span>
</div>
<script type="text/javascript">
	function search_member(str)
	{
		if (str.length==0)
		{ 
			document.getElementById("Hint").innerHTML="";
			return;
		}
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("Hint").innerHTML=xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","http://localhost/study_center/search_member.php?q="+str,true);
		xmlhttp.send();
	}
</script>