<div id="header">
	<!-- header_content_start -->
	<div id="header_content">

		<!-- header_top_start -->
		<div id="header_top">
			<div class="logo"><img src="http://localhost/study_center/images/swami.jpg" alt="logo" width="100px" height="80px"></div>
			<div class="title"><h2><br>Vivekanand Seva Mandal - Study Center</h2></div>
		</div>
		<!-- header_top_end -->

		<!-- header_bottom_start -->
		<?php
		if(isset($_SESSION['admin_id']))
		{
		?>
			<div id="header_bottom">
			<center>
				<ul align="center" id="menu">
					<li><a href="http://localhost/study_center/admin/index.php">Home</a></li>
					<li><a>New User</a>
						<ul class="sub-menu">
							<li><a href="http://localhost/study_center/admin/user/add_admin.php">Add User</a></li>
							<li><a href="http://localhost/study_center/admin/user/view_admin.php">View User</a></li>
						</ul>
					</li>
					<li><a>Update DB</a>
						<ul class="sub-menu">
							<li><a href="http://localhost/study_center/admin/update_database/add_branch.php">Branch</a></li>
							<li><a href="http://localhost/study_center/admin/update_database/add_college.php">College</a></li>
							<li><a href="http://localhost/study_center/admin/update_database/add_university.php">University</a></li>
							<li><a href="http://localhost/study_center/admin/update_database/view_fee_structure.php">Fee Structure</a></li>
							<li><a href="http://localhost/study_center/admin/update_database/view_admin_rights.php">Admin Rights</a></li>
						</ul>
					</li>
					<!-- <li><a href="http://localhost/study_center/admin/">Reports</a></li> -->
					<li><a href="http://localhost/study_center/member_entry.php">Study Center</a></li>
					<li><a href="http://localhost/study_center/admin/logout.php">Logout</a></li>
				</ul>
			</center>
			</div>
		<?php
		}
		?>
		<!-- header_bottom_end -->

	</div>
	<!-- header_content_end -->
</div>

