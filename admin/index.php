<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
	include '../db_config/db_config.php';
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Admin Home Page</title>

		<link rel="stylesheet" href="http://localhost/study_center/admin/css/admin_style.css" type="text/css" media="all" />
		<!-- <link rel="stylesheet" href="http://localhost/study_center/admin/css/bootstrap.css" type="text/css" media="all" /> -->

		<style type="text/css">

		.dashboard {
			margin: auto 15px ;
			padding: auto;
		}

		</style>
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container" style="height: 600px">

			<div class="jumbotron">
			<p>
				<h2>Welcome <?php echo $_SESSION['admin_user_name']; ?></h2>
			</p>
			</div>
			<?php 
			$total_members = mysql_num_rows(mysql_query("SELECT member_id FROM sc_member"));

			$active_members = mysql_num_rows(mysql_query("SELECT member_id FROM sc_member WHERE member_status = 1"));

			$locked_members = mysql_num_rows(mysql_query("SELECT member_id FROM sc_member WHERE member_status = 2"));

			$total_branch = mysql_num_rows(mysql_query("SELECT * FROM sc_branch"));

			$total_college = mysql_num_rows(mysql_query("SELECT * FROM sc_college"));

			$total_university = mysql_num_rows(mysql_query("SELECT * FROM sc_university"));
			?>
			<div class="dashboard">
				<p>Total Members : <b><?php echo $total_members + 1; ?></b></p><br/>
				<p>Total Active Members : <b><?php echo $active_members; ?></b></p><br/>
				<p>Total Locked Members : <b><?php echo $locked_members; ?></b></p><br/>
			</div>

			<div class="dashboard" >
				<p>Total Branches added : <b><a href="http://localhost/study_center/admin/update_database/add_branch.php"><?php echo $total_branch; ?></a></b></p><br/>
				<p>Total Colleges added : <b><a href="http://localhost/study_center/admin/update_database/add_college.php"><?php echo $total_college; ?></a></b></p><br/>
				<p>Total Universities added : <b><a href="http://localhost/study_center/admin/update_database/add_university.php"><?php echo $total_university; ?></a></b></p><br/>
			</div>

		</div>

		<!-- Footer_start -->
		<?php //include_once "templates/footer_template.php"; ?>
		<!-- Footer_end -->

	</body>
</html>