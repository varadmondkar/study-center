<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Update Fee Structure</title>
		<link rel="stylesheet" href="http://localhost/study_center/admin/css/admin_style.css" type="text/css" media="all" />
		<script type="text/javascript" src="javascript/add_admin.js"></script>
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "../templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container" style="height: 800px;">

			<div class="jumbotron">
			<p>
				<h2>Update Fee Structure</h2>
			</p>
			</div>
			
			<?php

				$fee_structure_id=$_POST['fee_structure_id'];
				$fee_amount=$_POST['fee_amount'];
				$fee_duration=$_POST['fee_duration'];
				
				include '../../db_config/db_config.php';

				$select1 = "SELECT fee_structure_id FROM sc_fee_structure WHERE fee_structure_id = '$fee_structure_id'";
				$result1 = mysql_query($select1) or die("ERROR1 : ".mysql_error());
				$count1 = mysql_num_rows($result1);

				if($count1==1)
				{
					$update="UPDATE sc_fee_structure SET fee_amount='$fee_amount',fee_duration='$fee_duration' WHERE fee_structure_id = '$fee_structure_id'";
					$result= mysql_query($update) or die("ERROR2 : ".mysql_error());
					
					echo "<center>Fee structure updated successfully.</center><br>";
				}
				else
				{
					echo "<center>Update fee structure operation failed.</center><br>";	
				}
				
				$select2 = "SELECT * FROM sc_fee_structure";
				$result2 = mysql_query($select2) or die("ERROR1 : ".mysql_error());
					
				echo "<center><table border='1'>";
				echo "<tr>";
				echo "<th>Fee Structure Id</th>";
				echo "<th>Fee Amount</th>";
				echo "<th>Fee Duration</th>";
				echo "<th>Action</th>";
				echo "</tr>";
					
				while($rows = mysql_fetch_array($result2))
				{
					extract($rows);
					echo "<tr>";
					echo "<td>$fee_structure_id</td>";
					echo "<td>$fee_amount/-</td>";
					echo "<td>$fee_duration</td>";
					echo "<td><a href='http://localhost/study_center/admin/update_database/update_fee_structure.php?fee_structure_id=$fee_structure_id'>Update</a></td>";
					echo "</tr>";
				}
				echo "</table></center>";
			?>
			
		</div>
		<!-- Content -->
		
		<!-- Footer_start -->
		<?php //include_once "../templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>
