<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Add University</title>
		<link rel="stylesheet" href="http://localhost/study_center/admin/css/admin_style.css" type="text/css" media="all" />
		<script type="text/javascript" src="javascript/add_admin.js"></script>
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "../templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container" style="height: 800px;">

			<div class="jumbotron">
			<p>
				<h2>Add University</h2>
			</p>
			</div>
			
			<?php

				$university_name=$_POST['university_name'];
				
				include '../../db_config/db_config.php';

				$select1 = "SELECT university_id FROM sc_university WHERE university_name = '$university_name'";
				$result1 = mysql_query($select1) or die("ERROR1 : ".mysql_error());
				$count1 = mysql_num_rows($result1);

				if($count1==0)
				{
					$insert="INSERT INTO sc_university (university_id,university_name)
					VALUES(NULL,'$university_name')";
					$result= mysql_query($insert) or die("ERROR2 : ".mysql_error());
								
					echo "<center><b>".$university_name." </b>University added successfully.</center><br>";
				}
				else
				{
					echo "<center><b>".$university_name." </b>University already exist.</center><br>";
				}
				
				$select2 = "SELECT * FROM sc_university";
				$result2 = mysql_query($select2) or die("ERROR4 : ".mysql_error());
					
				echo "<center><table border='1'>";
				echo "<tr>";
				echo "<th>University Id</th>";
				echo "<th>University Name</th>";
				echo "</tr>";
				
				while($rows = mysql_fetch_array($result2))
				{
					extract($rows);
					echo "<tr>";
					echo "<td>$university_id</td>";
					echo "<td>$university_name</td>";
					echo "</tr>";
				}
				echo "</table></center>";
			?>
			
		</div>
		<!-- Content -->
		
		<!-- Footer_start -->
		<?php //include_once "../templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>
