<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>View Fee Structure</title>
		<link rel="stylesheet" href="http://localhost/study_center/admin/css/admin_style.css" type="text/css" media="all" />
		<!-- <script type="text/javascript" src="javascript/add_admin.js"></script> -->
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "../templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container" style="height: 800px;">

			<div class="jumbotron">
			<p>
				<h2>View Fee Structure</h2>
			</p>
			</div>

			<?php

				include '../../db_config/db_config.php';

				$select1 = "SELECT * FROM sc_fee_structure";
				$result1 = mysql_query($select1) or die("ERROR1 : ".mysql_error());
				
				echo "<center><table border='1'>";
				echo "<tr>";
				echo "<th>Fee structure Id</th>";
				echo "<th>Fee amount</th>";
				echo "<th>Fee duration</th>";
				echo "<th>Action</th>";
				echo "</tr>";
					
				while($rows = mysql_fetch_array($result1))
				{
					extract($rows);
					echo "<tr>";
					echo "<td>$fee_structure_id</td>";
					echo "<td>$fee_amount/-</td>";
					echo "<td>$fee_duration</td>";
					echo "<td><a href='http://localhost/study_center/admin/update_database/update_fee_structure.php?fee_structure_id=$fee_structure_id'>Update</a></td>";
					echo "</tr>";
				}
					
				echo "</table></center>";
				
			?>
			
		</div>
		<!-- Content -->
		
		<!-- Footer_start -->
		<?php //include_once "../templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>
