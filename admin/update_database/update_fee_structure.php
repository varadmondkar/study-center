<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Update Fee Structure</title>
		<link rel="stylesheet" href="http://localhost/study_center/admin/css/admin_style.css" type="text/css" media="all" />
		<script>
			function validate()
			{
				var fee_structure_id=document.forms["update_fee_stucture_form"]["fee_structure_id"];
				var fee_amount=document.forms["update_fee_stucture_form"]["fee_amount"];
				var fee_duration=document.forms["update_fee_stucture_form"]["fee_duration"];
				
				if(isNumeric(fee_structure_id, "Please enter only numbers for fee structure id"))
				{
				if(isNumeric(fee_amount, "Please enter only numbers for fee amount"))
				{
				if(isAlphabet(fee_duration, "Please enter only alphabets for fee duration"))
				{
					return true;
				}}}
				
				return false;
			}
			function isAlphabet(element, message)
			{
				var alphaExp = /^[a-zA-Z\s]+$/;
				var str = element.value;
				str = str.trim();
				if(str.match(alphaExp)||element=='')
				{
					return true;
				}
				else
				{	alert(message);
					element.value="";
					element.focus();
					return false;
				}
			}
			function isNumeric(element,message)
			{
				var numExp=/^[0-9]+$/;
				if(element.value.match(numExp))
				{
					return true;
				}
				else
				{
					alert(message);
					element.value="";
					element.focus();
					return false;
				}
			}
			function isNumber(evt) {
				evt = (evt) ? evt : window.event;
				var charCode = (evt.which) ? evt.which : evt.keyCode;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					return false;
				}
				return true;
			}
		</script>
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "../templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container" style="height: 800px;">

			<div class="jumbotron">
			<p>
				<h2>Update Fee Structure</h2>
			</p>
			</div>

			<?php
				$fee_structure_id=$_GET['fee_structure_id'];
				include '../../db_config/db_config.php';

				$select1 = "SELECT * FROM sc_fee_structure WHERE fee_structure_id='$fee_structure_id'";
				$result1 = mysql_query($select1) or die("ERROR1 : ".mysql_error());
				
				while($rows = mysql_fetch_array($result1))
				{
					extract($rows);
				}
			?>

			<form name="update_fee_stucture_form" id="update_fee_stucture_form" action="confirm_update_fee_structure.php" onsubmit="return validate();" method="post">
				<table border="0" align="center">
					<tr>
						<td>Fee Structure Id:</td>
						<td><input type="text" name="fee_structure_id" value="<?php echo $fee_structure_id; ?>" readonly></td>
					</tr>
					<tr>
						<td>Fee Amount:</td>
						<td><input type="text" name="fee_amount" value="<?php echo $fee_amount; ?>" onkeypress="return isNumber(event)"></td>
					</tr>
					<tr>
						<td>Fee Duration:</td>
						<td><input type="text" name="fee_duration" value="<?php echo $fee_duration; ?>"></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: center;"><br/><input type="submit" value="UPDATE"><br></td>
					</tr>
				</table>
			</form>
		</div>
		<!-- Content -->

		<!-- Footer_start -->
		<?php include_once "../templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>