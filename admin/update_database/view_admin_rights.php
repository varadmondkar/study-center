<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>View Admin Rights</title>
		<link rel="stylesheet" href="http://localhost/study_center/admin/css/admin_style.css" type="text/css" media="all" />
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "../templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container" style="height: 800px;">

			<div class="jumbotron">
			<p>
				<h2>View Admin Rights</h2>
			</p>
			</div>
			<center>
			<table border="1">
				<tr>
					<th>Admin</th>
					<th>Member entry</th>
					<th>Add Member <br/> Lock member</th>
					<th>Member fee</th>
					<th>Add user <br/> View user</th>
					<th>Update Database</th>
					<th>Change <br/> admin rights</th>
					<th>Report generation</th>
					<th>Action</th>
				</tr>
				
				<form name="admin_rights_form" id="admin_rights_form" action="update_admin_rights.php" method="post">
				<tr>
					<td>Main admin</td>
				<?php
					include '../../db_config/db_config.php';
					$select1 = "SELECT * FROM sc_admin_rights WHERE admin_rights_id = 1";
					$result1 = mysql_query($select1) or die("ERROR1 : ".mysql_error());
					
					echo "<input type='hidden' name='admin_rights_id' value='1'>";
					
					while($rows = mysql_fetch_array($result1))
					{
						extract($rows);
						
						if($entry==1)
						{
							echo "<td><input type='checkbox' name='ar11' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar12' /></td>";
						}
						if($member==1)
						{
							echo "<td><input type='checkbox' name='ar21' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar22'/></td>";
						}
						if($fee==1)
						{
							echo "<td><input type='checkbox' name='ar31' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar32'/></td>";
						}
						if($user==1)
						{
							echo "<td><input type='checkbox' name='ar41' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar42'/></td>";
						}
						if($update_db==1)
						{
							echo "<td><input type='checkbox' name='ar51' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar52'/></td>";
						}
						if($update_user_rights==1)
						{
							echo "<td><input type='checkbox' name='ar61' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar62'/></td>";
						}
						if($report==1)
						{
							echo "<td><input type='checkbox' name='ar71' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar72'/></td>";
						}
					}
				?>
					<td><input type="submit" value="Update"></td>
				</tr>
				</form>
				
				<form name="admin_rights_form" id="admin_rights_form" action="update_admin_rights.php" method="post">
				<tr>
					<td>Local admin</td>
				<?php
					include '../../db_config/db_config.php';
					$select1 = "SELECT * FROM sc_admin_rights WHERE admin_rights_id = 2";
					$result1 = mysql_query($select1) or die("ERROR1 : ".mysql_error());
					
					echo "<input type='hidden' name='admin_rights_id' value='2'>";
					
					while($rows = mysql_fetch_array($result1))
					{
						extract($rows);
						
						if($entry==1)
						{
							echo "<td><input type='checkbox' name='ar11' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar12' /></td>";
						}
						if($member==1)
						{
							echo "<td><input type='checkbox' name='ar21' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar22'/></td>";
						}
						if($fee==1)
						{
							echo "<td><input type='checkbox' name='ar31' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar32'/></td>";
						}
						if($user==1)
						{
							echo "<td><input type='checkbox' name='ar41' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar42'/></td>";
						}
						if($update_db==1)
						{
							echo "<td><input type='checkbox' name='ar51' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar52'/></td>";
						}
						if($update_user_rights==1)
						{
							echo "<td><input type='checkbox' name='ar61' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar62'/></td>";
						}
						if($report==1)
						{
							echo "<td><input type='checkbox' name='ar71' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar72'/></td>";
						}
					}
				?>	
					<td><input type="submit" value="Update"></td>
				</tr>
				</form>
				
				<form name="admin_rights_form" id="admin_rights_form" action="update_admin_rights.php" method="post">
				<tr>
					<td>Guest</td>
				<?php
					include '../../db_config/db_config.php';
					$select1 = "SELECT * FROM sc_admin_rights WHERE admin_rights_id = 3";
					$result1 = mysql_query($select1) or die("ERROR1 : ".mysql_error());
					
					echo "<input type='hidden' name='admin_rights_id' value='3'>";
					
					while($rows = mysql_fetch_array($result1))
					{
						extract($rows);
						
						if($entry==1)
						{
							echo "<td><input type='checkbox' name='ar11' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar12' /></td>";
						}
						if($member==1)
						{
							echo "<td><input type='checkbox' name='ar21' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar22'/></td>";
						}
						if($fee==1)
						{
							echo "<td><input type='checkbox' name='ar31' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar32'/></td>";
						}
						if($user==1)
						{
							echo "<td><input type='checkbox' name='ar41' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar42'/></td>";
						}
						if($update_db==1)
						{
							echo "<td><input type='checkbox' name='ar51' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar52'/></td>";
						}
						if($update_user_rights==1)
						{
							echo "<td><input type='checkbox' name='ar61' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar62'/></td>";
						}
						if($report==1)
						{
							echo "<td><input type='checkbox' name='ar71' checked/></td>";
						}
						else
						{
							echo "<td><input type='checkbox' name='ar72'/></td>";
						}
					}
				?>
					<td><input type="submit" value="Update"></td>
				</tr>
				</form>
			</table>
			
			<br><br>
			<?php
				if(isset($_GET['m']))
				{
					if($_GET['m']==1)
					{
						echo "Admin rights updated.";
					}
				}
			?>
			</center>
			</form>
			
		</div>
		<!-- Content -->
		
		<!-- Footer_start -->
		<?php //include_once "../templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>
