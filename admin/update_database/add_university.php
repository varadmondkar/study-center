<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Add University</title>
		<link rel="stylesheet" href="http://localhost/study_center/admin/css/admin_style.css" type="text/css" media="all" />
		<script>
			function validate()
			{
				var university_name=document.forms["add_university_form"]["university_name"];
				
				if(isAlphabet(university_name, "Please enter only alphabets for university name"))
				{
					return true;
				}
				
				return false;
			}
			function isAlphabet(element, message)
			{
				var alphaExp = /^[a-zA-Z\s]+$/;
				var str = element.value;
				str = str.trim();
				if(str.match(alphaExp)||element=='')
				{
					return true;
				}
				else
				{	alert(message);
					element.value="";
					element.focus();
					return false;
				}
			}
		</script>
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "../templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container" style="height: 800px;">

			<div class="jumbotron">
			<p>
				<h2>Add University</h2>
			</p>
			</div>
			
			<form name="add_university_form" id="add_university_form" action="confirm_add_university.php" onsubmit="return validate();" method="post">
				<table border="0" align="center">
					<tr>
						<td>University Name:</td>
						<td><input type="text" name="university_name" placeholder=" University name"></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: center;"><br/><input type="submit" value="ADD UNIVERSITY"><br></td>
					</tr>
				</table>
			</form>
			<br>
			<?php

				include '../../db_config/db_config.php';
				
				$select1 = "SELECT * FROM sc_university";
				$result1 = mysql_query($select1) or die("ERROR1 : ".mysql_error());
				
				echo "<center><table border='1'>";
				echo "<tr>";
				echo "<th>University id</th>";
				echo "<th>University name</th>";
				echo "</tr>";
					
				while($rows = mysql_fetch_array($result1))
				{
					extract($rows);
					echo "<tr>";
					echo "<td>$university_id</td>";
					echo "<td>$university_name</td>";
					echo "</tr>";
				}
					
				echo "</table></center>";			
			?>
		</div>
		<!-- Content -->
		
		<!-- Footer_start -->
		<?php //include_once "../templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>