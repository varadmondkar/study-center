<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Add College</title>
		<link rel="stylesheet" href="http://localhost/study_center/admin/css/admin_style.css" type="text/css" media="all" />
		<script>
			function validate()
			{
				var college_name=document.forms["add_college_form"]["college_name"];
				
				if(isAlphabet(college_name, "Please enter only alphabets for college name"))
				{
					return true;
				}
				
				return false;
			}
			function isAlphabet(element, message)
			{
				var alphaExp = /^[a-zA-Z\s]+$/;
				if(element.value.match(alphaExp))
				{
					return true;
				}
				else
				{	alert(message);
					element.value="";
					element.focus();
					return false;
				}
			}
		</script>
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "../templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container">

			<div class="jumbotron">
			<p>
				<h2>Add College</h2>
			</p>
			</div>
			
			<form name="add_college_form" id="add_college_form" action="confirm_add_college.php" onsubmit="return validate();" method="post">
				<table border="0" align="center">
					<tr>
						<td>College Name:</td>
						<td><input type="text" name="college_name" placeholder=" College name"></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: center;"><br/><input type="submit" value="ADD COLLEGE"><br></td>
					</tr>
				</table>
			</form>
			<br>
			<?php

				include '../../db_config/db_config.php';
				
				$select1 = "SELECT * FROM sc_college";
				$result1 = mysql_query($select1) or die("ERROR1 : ".mysql_error());
				
				echo "<center><table border='1'>";
				echo "<tr>";
				echo "<th>College id</th>";
				echo "<th>College name</th>";
				echo "</tr>";
					
				while($rows = mysql_fetch_array($result1))
				{
					extract($rows);
					echo "<tr>";
					echo "<td>$college_id</td>";
					echo "<td>$college_name</td>";
					echo "</tr>";
				}
					
				echo "</table></center>";			
			?>
		</div>
		<!-- Content -->
		
		<!-- Footer_start -->
		<?php //include_once "../templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>