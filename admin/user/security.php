<?php 
	session_start(); 
	if(isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/admin/');
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Admin Login</title>
		<link rel="stylesheet" href="http://localhost/study_center/css/main.css" type="text/css" media="all" />
		<style>
		body {
			background: -webkit-linear-gradient(#F2F2F2, #F2F2F2, #F2F2F2); /* For Safari */
			background: -o-linear-gradient(#F2F2F2, #F2F2F2, #F2F2F2); /* For Opera 11.1 to 12.0 */
			background: -moz-linear-gradient(#F2F2F2, #F2F2F2, #F2F2F2); /* For Firefox 3.6 to 15 */
			background: linear-gradient(#F2F2F2, #F2F2F2, #F2F2F2); /* Standard syntax (must be last) */
		}
		input[type=text], input[type=password] {
			font-family: 'Verdana', Tahoma, Lucida Grande, sans-serif;
			font-size: 14px;
			margin: 5px;
			padding: 0 10px;
			width: 250px;
			height: 34px;
			color: #404040;
			background: white;
			border: 1px solid;
			border-color: #c4c4c4 #d1d1d1 #d4d4d4;
			border-radius: 2px;
			outline: 5px solid #eff4f7;
			-moz-outline-radius: 3px;
			-webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.12);
			box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.12);
		}
		input[type=text]:focus, input[type=password]:focus {
			border-color: #848484;/* #7dc9e2; */
			outline-color: #D8D8D8;/* #dceefc; */
			outline-offset: 0;
		}

		input[type=submit] {
			padding: 2px 130px;
			line-height: 14px;
			height: 29px;
			font-size: 14px;
			font-weight: bold;
			color: #555;
			text-shadow: 0 1px #e3f1f1;
			background: #D3D9D4;
			border: 1px solid;
			border-color: #b4ccce #b3c0c8 #9eb9c2;
			border-radius: 6px;
			outline: 0;
			-webkit-box-sizing: content-box;
			-moz-box-sizing: content-box;
			box-sizing: content-box;
			background-image: -webkit-linear-gradient(top, #FAFAFA, #D3D9D4);
			background-image: -moz-linear-gradient(top, #FAFAFA, #D3D9D4);
			background-image: -o-linear-gradient(top, #FAFAFA, #D3D9D4);
			background-image: linear-gradient(to bottom, #FAFAFA, #D3D9D4);
			-webkit-box-shadow: inset 0 1px white, 0 1px 2px rgba(0, 0, 0, 0.15);
			box-shadow: inset 0 1px white, 0 1px 2px rgba(0, 0, 0, 0.15);
		}
		input[type=submit]:active {
			background: #D3D9D4;
			border-color: #9eb9c2 #b3c0c8 #b4ccce;
			-webkit-box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.2);
			box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.2);
		}

		.lt-ie9 input[type=text], .lt-ie9 input[type=password] {
			line-height: 34px;
		}
		</style>
	</head>

	<body>
		<!-- header_start -->
		<?php //include_once "../../templates/login_header_template.php"; ?>
		<!-- header_end -->

		<!--<div id="container" style="height: 700px;">-->
			<br><br><br><br><br>
			
			<?php
				if(isset($_POST['forgot_password_submit']))
				{
					include '../../db_config/db_config.php';

					$user_name 		= $_POST['user_name'];
					$mobile_number 	= $_POST['mobile_number'];

					$select1 = "SELECT * FROM sc_admin WHERE admin_user_name = '$user_name' AND admin_mobile_number = '$mobile_number'";
					$result1 = mysql_query($select1) or die("ERROR1 ".mysql_error());
					$count 	= mysql_num_rows($result1);
					while($rows = mysql_fetch_array($result1))
					{
						extract($rows);
					}

					if($count ==1)
					{
						$select2 = "SELECT * FROM sc_security_question WHERE sc_security_question_id = '$security_question_id'";
						$result2 = mysql_query($select2) or die("ERROR2 ".mysql_error());
						while($rows = mysql_fetch_array($result2))
						{
							extract($rows);
						}

						echo "<center>";
						echo "<form name='security_form' id='security_form' action='security.php' method='post'>";
						echo "<table border='0' align='center'>";
						echo "<tr>";
						echo "<td><input type='hidden' name='admin_id' value='".$admin_id."'></td>";
						echo "</tr>";
						echo "<tr>";
						echo "<td>Security Question:</td>";
						echo "<td><input type='text' name='security_question' value='".$security_question."' readonly></td>";
						echo "<td><input type='hidden' name='security_question_id' value='".$security_question_id."'></td>";
						echo "</tr>";
						echo "<tr>";
						echo "<td>Security answer:</td>";
						echo "<td><input type='text' name='security_answer'  placeholder='Answer above security question'></td>";
						echo "</tr>";
						echo "<tr>";
						echo "<td colspan='2' style='text-align: center;'><br/><input type='submit' name='security_answer_submit' value='SUBMIT ANSWER'><br></td>";
						echo "</tr>";
						echo "</table>";
						echo "</form>";
						echo "</center>";
						if(isset($_GET['success']))
						{
							$success = $_GET['success'];
							if($success == 0)
								echo "<div style='text-align:center;border:2px;color: red'>Username Password combination doesn't match</div>";
						}
					}
					else
					{
						header('Location: forgot_password.php?success=0');
					}
				}

				if(isset($_POST['security_answer_submit']))
				{

					$admin_id 		 		= $_POST['admin_id'];
					$security_question_id 	= $_POST['security_question_id'];
					$security_answer 		= $_POST['security_answer'];
					
					include '../../db_config/db_config.php';

					$select3 = "SELECT * FROM sc_admin WHERE admin_id = '$admin_id' AND security_question_id = '$security_question_id' AND security_answer = '$security_answer'";
					$result3 = mysql_query($select3) or die("ERROR3 ".mysql_error());
					$member_count 	= mysql_num_rows($result3);
					while($rows = mysql_fetch_array($result3))
					{
						extract($rows);
					}

					if($member_count == 1)
					{
						echo "<center>";
						echo "<form name='new_password_form' id='new_password_form' action='security.php' method='post'>";
						echo "<table border='0' align='center'>";
						echo "<tr>";
						echo "<td><input type='hidden' name='admin_id' value='".$admin_id."'></td>";
						echo "</tr>";
						echo "<tr>";
						echo "<td>New Password:</td>";
						echo "<td><input type='password' name='new_password'></td>";
						echo "</tr>";
						echo "<tr>";
						echo "<td>Confirm New Password:</td>";
						echo "<td><input type='password' name='confirm_new_password'></td>";
						echo "</tr>";
						echo "<tr>";
						echo "<td colspan='2' style='text-align: center;'><br/><input type='submit' name='submit_password' value='SUBMIT'><br></td>";
						echo "</tr>";
						echo "</table>";
						echo "</form>";
						echo "</center>";
					}
					else
					{
						header('Location: forgot_password.php?success=-1');
					}
				}

				if(isset($_POST['submit_password']))
				{

					$admin_id 		 			= $_POST['admin_id'];
					$new_password 				= $_POST['new_password'];
					$confirm_new_password 		= $_POST['confirm_new_password'];
					if($new_password != $confirm_new_password)
					{
						header('Location: security.php?match=0');
					}
					else
					{
						$new_password_hash  = md5($new_password);
						
						include '../../db_config/db_config.php';

						$update4 = "UPDATE sc_admin SET admin_password = '$new_password_hash' WHERE admin_id = '$admin_id'";
						$result4 = mysql_query($update4) or die("ERROR4 ".mysql_error());
						
						header('Location: security.php?match=1');
					}

				}

				if(isset($_GET['match']))
				{
					$match = $_GET['match'];

					if($match == 0)
					echo "<center>Password doesn't match <a href='http://localhost/study_center/'>Try again</a></center>";
					else 
					echo "<center>Password successfully changed <a href='http://localhost/study_center/'>Login</a></center>";
				}
			?>


		<!--</div>-->

		<!-- Footer_start -->
		<?php //include_once "../../templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>
