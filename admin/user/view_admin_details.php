<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>View Admin Details</title>
		<link rel="stylesheet" href="http://localhost/study_center/admin/css/admin_style.css" type="text/css" media="all" />
		<script type="text/javascript" src="http://localhost/study_center/admin/javascript/view_admin.js"></script>
		<script type="text/javascript">
			function isNumber(evt) {
				evt = (evt) ? evt : window.event;
				var charCode = (evt.which) ? evt.which : evt.keyCode;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					return false;
				}
				return true;
			}
		</script>
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "../templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container" style="height: 800px;">

			<div class="jumbotron">
			<p>
				<h2>View Users</h2>
			</p>
			</div>
			
			<?php
		
				if (isset($_GET['admin_id']))
				{
					$admin_id = $_GET['admin_id'];

					include '../../db_config/db_config.php';

					$select = "SELECT * FROM sc_admin WHERE admin_id = '$admin_id'";
					$result1 = mysql_query($select) or die("ERROR1 ".mysql_error());
					$count = mysql_num_rows($result1) or die("ERROR2 ".mysql_error());

					while($rows = mysql_fetch_array($result1))
					{
						extract($rows);
					}
					
					if($admin_type==1)
					{
						$admin_type = "Main Admin";
					}
					else if($admin_type==2)
					{
						$admin_type = "Local Admin";
					}
					else if($admin_type==3)
					{
						$admin_type = "Guest Admin";
					}

					echo "<form name='view_admin_form' id='view_admin_form' action='update_admin.php' onsubmit='return validate()' method='post'>
							<table border='0' align='center'>
								<tr>
									<td>Username:</td>
									<td><input type='text' name='user_name' value='".$admin_user_name."' readonly></td>
								</tr>
								<tr>
									<td>Admin Name:</td>
									<td><input type='text' name='admin_name' value='".$admin_name."'></td>
								</tr>
								<tr>
									<td>Admin Type:</td>
									<td><input type='text' name='admin_type' value='".$admin_type."' readonly></td>
								</tr>
								<tr>
									<td>Gender:</td>
									<td><input type='text' name='admin_gender' value='".$admin_gender."' readonly></td>
								</tr>
								<tr>
									<td>Mobile no:</td>
									<td><input type='text' name='admin_mobile_number' value='".$admin_mobile_number."' onkeypress='return isNumber(event)'></td>
								</tr>
								<tr>
									<td>E-mail id:</td>
									<td><input type='text' name='admin_email_id' value='".$admin_email_id."'></td>
								</tr>";
								if($admin_id==$_SESSION['admin_id'])
								{
								echo "<tr>
										<td><a href='change_password.php'>Change password</a></td>
									</tr>";	
								}
							echo "<tr>
									<td colspan='2' style='text-align: center;'><br/><input type='submit' value='UPDATE'><br></td>
								</tr>
							</table>
						</form>";
				}
			?>
			
		</div>
		<!-- Content -->
		
		<!-- Footer_start -->
		<?php //include_once "../templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>