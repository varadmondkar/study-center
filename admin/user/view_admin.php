<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>View Admin</title>
		<link rel="stylesheet" href="http://localhost/study_center/admin/css/admin_style.css" type="text/css" media="all" />
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "../templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container" style="height: 800px;">

			<div class="jumbotron">
			<p>
				<h2>View Users</h2>
			</p>
			</div>
			
			<?php

				include '../../db_config/db_config.php';
				
				$select1 = "SELECT * FROM sc_admin";
				$result1 = mysql_query($select1) or die("ERROR1 : ".mysql_error());
				
				echo "<center><table border='1'>";
				echo "<tr>";
				echo "<th>Username</th>";
				echo "<th>Admin Type</th>";
				echo "<th>Mobile</th>";
				echo "<th>Email</th>";
				echo "<th>View Details</th>";
				echo "</tr>";
					
				while($rows = mysql_fetch_array($result1))
				{
					extract($rows);
					echo "<tr>";
					echo "<td>$admin_user_name</td>";
					if($admin_type==1)
					{
						$admin_type = "Main Admin";
					}
					else if($admin_type==2)
					{
						$admin_type = "Local Admin";
					}
					else if($admin_type=3)
					{
						$admin_type = "Guest Admin";
					}
					echo "<td>$admin_type</td>";
					echo "<td>$admin_mobile_number</td>";
					echo "<td>$admin_email_id</td>";
					echo "<td><a href='http://localhost/study_center/admin/user/view_admin_details.php?admin_id=$admin_id'>View/Update</a></td>";
					echo "</tr>";
				}
					
				echo "</table></center>";			
			?>
			
		</div>
		<!-- Content -->
		
		<!-- Footer_start -->
		<?php //include_once "../templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>