<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Add Admin</title>
		<link rel="stylesheet" href="http://localhost/study_center/admin/css/admin_style.css" type="text/css" media="all" />
		<script type="text/javascript" src="http://localhost/study_center/admin/javascript/add_admin.js"></script>
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "../templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container" style="height: 800px;">

			<div class="jumbotron">
			<p>
				<h2>Add New User</h2>
			</p>
			</div>
			
			<form name="add_admin_form" id="add_admin_form" action="confirm_add_admin.php" onsubmit="return validate();" method="post">
				<table border="0" align="center">
					<tr>
						<td>Username:</td>
						<td><input type="text" name="user_name" placeholder=" Username"></td>
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type="password" name="password" placeholder=" Password"></td>
					</tr>
					<tr>
						<td>Admin Name:</td>
						<td><input type="text" name="admin_name" placeholder=" Admin name"></td>
					</tr>
					<tr>
						<td>Gender:</td>
						<td>
							<select name="admin_gender" style="width:150px;">
								<option value=""> Gender</option>
								<option value="Male">Male</option>
								<option value="Female">Female</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Mobile Number:</td>
						<td><input type="text" name="admin_mobile_number" placeholder=" Mobile number"></td>
					</tr>
					<tr>
						<td>Admin Type:</td>
						<td>
							<select name="admin_type" style="width:150px;">
								<option value=""> Admin type</option>
								<option value="1">Main Admin</option>
								<option value="2">Local Admin</option>
								<option value="3">Guest</option>
						</td>
					</tr>
					<tr>
						<td>Security Question:</td>
						<td>
							<?php 
							include '../../db_config/db_config.php';
							$query = "SELECT * FROM sc_security_question";
							$result = mysql_query($query);
							?>
							<select name="security_question" style="width:150px;">
								<option value=""> Question</option>
								<?php
								while ($security_question = mysql_fetch_array($result))
								{
									extract($security_question);
									echo "<option value='".$sc_security_question_id."'>".$security_question."</option>";
								}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Security Answer:</td>
						<td><input type="text" name="security_answer" placeholder=" Security answer"></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: center;"><br/><input type="submit" value="ADD USER"><br></td>
					</tr>
				</table>
			</form>
		</div>
		<!-- Content -->
		
		<!-- Footer_start -->
		<?php //include_once "../templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>