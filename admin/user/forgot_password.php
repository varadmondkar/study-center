<?php 
	session_start(); 
	if(isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/admin/');
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Admin Login</title>
		<link rel="stylesheet" href="http://localhost/study_center/css/main.css" type="text/css" media="all" />
		<style>
		body {
			background: -webkit-linear-gradient(#F2F2F2, #F2F2F2, #F2F2F2); /* For Safari */
			background: -o-linear-gradient(#F2F2F2, #F2F2F2, #F2F2F2); /* For Opera 11.1 to 12.0 */
			background: -moz-linear-gradient(#F2F2F2, #F2F2F2, #F2F2F2); /* For Firefox 3.6 to 15 */
			background: linear-gradient(#F2F2F2, #F2F2F2, #F2F2F2); /* Standard syntax (must be last) */
		}
		input[type=text], input[type=password] {
			font-family: 'Verdana', Tahoma, Lucida Grande, sans-serif;
			font-size: 14px;
			margin: 5px;
			padding: 0 10px;
			width: 200px;
			height: 34px;
			color: #404040;
			background: white;
			border: 1px solid;
			border-color: #c4c4c4 #d1d1d1 #d4d4d4;
			border-radius: 2px;
			outline: 5px solid #eff4f7;
			-moz-outline-radius: 3px;
			-webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.12);
			box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.12);
		}
		input[type=text]:focus, input[type=password]:focus {
			border-color: #848484;/* #7dc9e2; */
			outline-color: #D8D8D8;/* #dceefc; */
			outline-offset: 0;
		}

		input[type=submit] {
			padding: 2px 120px;
			line-height: 14px;
			height: 29px;
			font-size: 14px;
			font-weight: bold;
			color: #555;
			text-shadow: 0 1px #e3f1f1;
			background: #D3D9D4;
			border: 1px solid;
			border-color: #b4ccce #b3c0c8 #9eb9c2;
			border-radius: 6px;
			outline: 0;
			-webkit-box-sizing: content-box;
			-moz-box-sizing: content-box;
			box-sizing: content-box;
			background-image: -webkit-linear-gradient(top, #FAFAFA, #D3D9D4);
			background-image: -moz-linear-gradient(top, #FAFAFA, #D3D9D4);
			background-image: -o-linear-gradient(top, #FAFAFA, #D3D9D4);
			background-image: linear-gradient(to bottom, #FAFAFA, #D3D9D4);
			-webkit-box-shadow: inset 0 1px white, 0 1px 2px rgba(0, 0, 0, 0.15);
			box-shadow: inset 0 1px white, 0 1px 2px rgba(0, 0, 0, 0.15);
		}
		input[type=submit]:active {
			background: #D3D9D4;
			border-color: #9eb9c2 #b3c0c8 #b4ccce;
			-webkit-box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.2);
			box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.2);
		}

		.lt-ie9 input[type=text], .lt-ie9 input[type=password] {
			line-height: 34px;
		}
		</style>
		<script type="text/javascript">
			function isNumber(evt) {
				evt = (evt) ? evt : window.event;
				var charCode = (evt.which) ? evt.which : evt.keyCode;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					return false;
				}
				return true;
			}

			function validate()
			{
				var user_name=document.forms["forgot_password_form"]["user_name"];
				var mobile_no=document.forms["forgot_password_form"]["mobile_no"];
	
				if(isAlphanumeric(user_name, "Please enter only numbers & letters for username"))
				{
				if(lengthRestriction(mobile_no, 10, "Please enter 10 digit mobile no"))
				{
					return true;
				}}
					return false;
			}

			function isAlphanumeric(element,message)
			{
				var alphanumExp = /^[0-9a-zA-Z\s,.-]+$/;
				if(element.value.match(alphanumExp))
				{
					return true;
				}
				else
				{
					alert(message);
					element.focus();
					return false;
				}
			}////end of function isAlphanumeric()
			function lengthRestriction(element, upperlimit, message)
			{
				var  len=element.value;
				var fieldname=element;
				if(len.length == upperlimit)
				{
					return true;
				}
				else if(len.length != upperlimit)
				{
					alert(message);
					element.value="";
					element.focus();
					return false;
				}
			}////end of function lengthRestriction()
		</script>
	</head>

	<body>
		<!-- header_start -->
		<?php //include_once "../../templates/login_header_template.php"; ?>
		<!-- header_end -->

		<!--<div id="container" style="height: 700px;">-->
			<br><br><br><br><br>
			<center>
			<div class="login">
				<h1>Forgot Password</h1>
				<form name='forgot_password_form' id='forgot_password_form' action='security.php' onsubmit='return validate();' method='post'>

							<p><input type='text' name='user_name' placeholder='Username'></p>
						
							<p><input type='text' name='mobile_number' placeholder='10 Digit Mobile Number' onkeypress='return isNumber(event)'></p>
							<p class="submit"><input type='submit' name='forgot_password_submit' value='SUBMIT'></p>
				</form>
				<br>
				<a href="http://localhost/study_center/index.php" style="text-decoration:none"><-Go to login page</a>
			</div>
			<?php
				if(isset($_GET['success']))
				{
					$success = $_GET['success'];
					if($success == 0)
						echo "<br><font style='text-align:center' color='red' size='4'>Username-Mobile number combination doesn't match</font>";
					else if($success == -1)
						echo "<br><font style='text-align:center' color='red' size='4'>Invalid security details</font>";
				}
			?>
			</center>



		<!--</div>-->

		<!-- Footer_start -->
		<?php //include_once "../../templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>

					
