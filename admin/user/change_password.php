<?php 
	session_start(); 
	if(!isset($_SESSION['admin_id']))
	{
		header('Location: http://localhost/study_center/');
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Change password</title>
		<link rel="stylesheet" href="http://localhost/study_center/admin/css/admin_style.css" type="text/css" media="all" />
		<script type="text/javascript">
			function validate()
			{
				var old_password=document.forms["change_password_form"]["old_password"];
				var new_password=document.forms["change_password_form"]["new_password"];

				if(notEmpty(old_password, "Invalid old password"))//old password should have length >5 
				{
				if(notEmpty(new_password, "Please enter strong password. It should have more than 5 characters."))
				{
					return true;
				}}
					return false;
			}

			function notEmpty(element, message)
			{
				if(element.value.length <=5)
				{
					alert(message);
					element.focus();
					return false;
				}
					return true;
			}
		</script>
	</head>

	<body>
		<!-- header_start -->
		<?php include_once "../templates/header_template.php"; ?>
		<!-- header_end -->

		<div id="container" style="height: 800px;">

			<div class="jumbotron">
			<p>
				<h2>Change password</h2>
			</p>
			</div>
<?php
	echo "<center>
	<form name='change_password_form' id='change_password_form' action='change_password.php' onsubmit='return validate();' method='post'>
		<table border='0' align='center'>
			<tr>
				<td>Old password:</td>
				<td><input type='password' name='old_password' placeholder='Old password'></td>
			</tr>
			<tr>
				<td>New password:</td>
				<td><input type='password' name='new_password' placeholder='New password'></td>
			</tr>
			<tr>
				<td colspan='2' style='text-align: center;'><br/><input type='submit' name='change_password_submit' value='SUBMIT'><br></td>
			</tr>
		</table>
	</form></center><br/>";
	if(isset($_GET['success']))
	{
		$success = $_GET['success'];
		if($success == 1)
			echo "<div style='text-align:center;border:2px;color: green'>Password changed successfully</div>";
		else
			echo "<div style='text-align:center;border:2px;color: red'>Failed to change password</div>";
	}
?>
<?php
if(isset($_POST['change_password_submit']))
{

	if(strlen($_POST['new_password']) < 6)
	{
		echo "<div style='text-align:center;border:2px;color: red'>Length of new password must be more than 5 characters.</div>";
	}
	else
	{
		include '../../db_config/db_config.php';

		$admin_id = $_SESSION['admin_id'];
		$old_password_query = "SELECT * FROM sc_admin WHERE admin_id = ".$admin_id;
		$old_password_result = mysql_query($old_password_query) or die("ERROR 0 : ".mysql_error());
		while($rows = mysql_fetch_array($old_password_result))
			extract($rows);

		$old_password = $_POST['old_password'];
		$new_password = $_POST['new_password'];

		$old_password_hash = md5($old_password);
		$new_password_hash = md5($new_password);

		if($old_password_hash != $admin_password)
		{
			echo "<div style='text-align:center;border:2px;color: red'>Incorrect Old Password</div>";
		}
		else
		{
			$select = "SELECT * FROM sc_admin WHERE admin_id='$admin_id'";
			$result = mysql_query($select) or die("ERROR 1 : ".mysql_error());
			$count = mysql_num_rows($result);

			if($count==1)
			{
				$update = "UPDATE sc_admin SET admin_password='$new_password_hash' WHERE admin_id='$admin_id' and admin_password='$old_password_hash'";
				$result1 = mysql_query($update) or die("ERROR 2 ".mysql_error());

				if($result1)
				{
					header('Location: change_password.php?success=1');
				}
				else
				{
					header('Location: change_password.php?success=0');
				}
			}
			else
			{
				header('Location: change_password.php?success=0');
			}
		}
	}
}
?>
		</div>
		<!-- Content -->
		
		<!-- Footer_start -->
		<?php //include_once "../templates/footer_template.php"; ?>
		<!-- Footer_end -->
	</body>
</html>