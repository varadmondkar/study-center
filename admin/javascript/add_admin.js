function validate()
{
	var user_name=document.forms["add_admin_form"]["user_name"];
	var password=document.forms["add_admin_form"]["password"];
	var admin_name=document.forms["add_admin_form"]["admin_name"];
	var admin_gender=document.forms["add_admin_form"]["admin_gender"];
	var admin_mobile_number=document.forms["add_admin_form"]["admin_mobile_number"];
	var admin_type=document.forms["add_admin_form"]["admin_type"];
	var security_question=document.forms["add_admin_form"]["security_question"];
	var security_answer=document.forms["add_admin_form"]["security_answer"];
		
	if(isAlphanumeric(user_name, "Please enter only numbers & letters for username"))
	{
	if(notEmpty(password, "Please enter strong password"))
	{
	if(isAlphabet(admin_name, "Please enter only alphabets for admin name"))
	{
	if(isOptionSelected(admin_gender, "Please select admin gender"))
	{
	if(lengthRestriction(admin_mobile_number, 10, "Please enter a valid Mobile number"))
	{
	if(isOptionSelected(admin_type, "Please select admin type"))
	{
	if(isOptionSelected(security_question, "Please select Security question"))
	{
	if(isAlphanumeric(security_answer, "Please enter only numbers & letters for Security answer"))
	{
		return true;
	}}}}}}}}
		return false;
}//end of function validate()

function notEmpty(element, message)
{
	if(element.value.length <=5)
	{
		alert(message);
		element.focus();
		return false;
	}
	return true;
}

function isAlphabet(element, message)
{
	var alphaExp = /^[a-zA-Z\s]+$/;
	if(element.value.match(alphaExp))
	{
		return true;
	}
	else
	{	alert(message);
		element.value="";
		element.focus();
		return false;
	}
}//end of function isAlphabet()

function isEmptyAlphabet(element, message)
{
	var alphaExp = /^[a-zA-Z]+$/;
	var element1 = element.value;
	if(element.value.match(alphaExp)||element1=='')
	{
		return true;
	}
	else
	{
		alert(message);
		element.focus();
		return false;
	}
}

function isOptionSelected(element,message)
{
	if(element.value==0)
	{
		alert(message);
		element.focus();
		return false;
	}
	else 
	{
		return true;
	}
}////end of function isOptionSelected()

function isRadiobuttonClicked(element,message)
{
	if ((member_form.gender[0].checked== false) && (member_form.gender[1].checked== false) )
	{
		alert(message);
		return false;
	}
	else
	{
		return true;
	}
}////end of function isRadiobuttonClicked()

function isAlphanumeric(element,message)
{
	var alphanumExp = /^[0-9a-zA-Z\s,.-]+$/;
	if(element.value.match(alphanumExp))
	{
		return true;
	}
	else
	{
		alert(message);
		element.focus();
		return false;
	}
}////end of function isAlphanumeric()

function isEmptyAlphanumeric(element, message)
{
	var alphanumExp = /^[0-9a-zA-Z\s,.-]+$/;
	var element1 = element.value;
	if(element.value.match(alphanumExp)||element1=='')
	{
		return true;
	}
	else
	{
		alert(message);
		element.focus();
		return false;
	}
}

function isNumeric(element,message)
{
	var numExp=/^[0-9]+$/;
	if(element.value.match(numExp))
	{
		return true;
	}
	else
	{
		alert(message);
		element.value="";
		element.focus();
		return false;
	}
}////end of function isNumeric()

function isEmptyNumeric(element, message)
{
	var numExp = /^[0-9]+$/;
	var element1 = element.value;
	if(element.value.match(numExp)||element1=='')
	{
		return true;
	}
	else
	{
		alert(message);
		element.focus();
		return false;
	}
}

function lengthRestriction(element, upperlimit, message)
{
	var  len=element.value;
	var fieldname=element;
	if(len.length == upperlimit)
	{
		return true;
	}
	else if(len.length != upperlimit)
	{
		alert(message);
		element.value="";
		element.focus();
		return false;
	}
}////end of function lengthRestriction()

function emailValidator(element, message)
{
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(element.value.match(emailExp))
	{
		return true;
	}
	else
	{
		alert(message);
		element.value="";
		element.focus();
		return false;
	}
}//end of function emailValidator()