function validate()
{
	var admin_name=document.forms["view_admin_form"]["admin_name"];
	var admin_mobile_number=document.forms["view_admin_form"]["admin_mobile_number"];
	var admin_email_id=document.forms["view_admin_form"]["admin_email_id"];
			
	if(isAlphabet(admin_name, "Please enter only alphabets for Admin name"))
	{
	if(lengthRestriction(admin_mobile_number, 10, "Please enter a valid Mobile number"))
	{
	if(emailValidator(admin_email_id, "Please enter a valid Email address"))
	{
		return true;
	}}}
		return false;
}//end of function validate()

function isAlphabet(element, message)
{
	var alphaExp = /^[a-zA-Z\s]+$/;
	if(element.value.match(alphaExp))
	{
		return true;
	}
	else
	{	alert(message);
		element.focus();
		return false;
	}
}//end of function isAlphabet()

function emailValidator(element, message)
{
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(element.value.length==0)
	{
		return true;
	}
	else if(element.value.length!=0)
	{
		if(element.value.match(emailExp))
		{
			return true;
		}
		else
		{
			alert(message);
			element.focus();
			return false;
		}
	}
}//end of function emailValidator()

function lengthRestriction(element, upperlimit, message)
{
	var  len=element.value;
	var fieldname=element;
	if(len.length == upperlimit)
	{
		return true;
	}
	else if(len.length != upperlimit)
	{
		alert(message);
		element.value="";
		element.focus();
		return false;
	}
}////end of function lengthRestriction()