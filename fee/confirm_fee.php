<?php 
session_start(); 
if(!isset($_SESSION['admin_id']))
{
	header('Location: http://localhost/study_center/');
}
include '../db_config/db_config.php';
date_default_timezone_set ("Asia/Calcutta");

$admin_id=$_SESSION['admin_id'];
$member_id = $_POST['member_id'];
$fee_plan = $_POST['fee_plan'];
$receipt_no = $_POST['receipt_no'];
$booklet_no = $_POST['booklet_no'];
$fee_date = $_POST['fee_date']; // Added for time being
//$fee_history_id = $receipt_no.$booklet_no;

if($receipt_no==NULL AND $booklet_no==NULL AND $fee_plan == 0)
{
	header('Location: ../member_fee.php?id='.$member_id.'&no_receipt_booklet=1');
}
else
{
	$extract_fee_amount = "SELECT fee_amount from sc_fee_structure WHERE fee_duration = '$fee_plan'";
	$extract_result = mysql_query($extract_fee_amount) or die("ERROR 1 :".mysql_error());
	while($amnt=mysql_fetch_array($extract_result))
		extract($amnt);

	$current_date = date('Y-m-d');
	$current_time = date('h:i:s');

	if($fee_plan == 'daily') 
	{
		$due_date = $fee_date; //date('Y-m-d');
	}
	if($fee_plan == 'monthly') 
	{
		$due_date = date('Y-m-d',strtotime($fee_date.' +1 month -1 day'));
	}
	if($fee_plan == 'quarterly') 
	{
		$due_date = date('Y-m-d',strtotime($fee_date.' +3 month -1 day'));
	}
	if($fee_plan == 'half yearly') 
	{
		$due_date = date('Y-m-d',strtotime($fee_date.' +6 month -1 day'));
	}

	$add_fee_query = "INSERT INTO sc_fee(fee_member_id,fee_plan,fee_receipt_number,fee_booklet_number,fee_payment_date,fee_due_date) 
	VALUES('$member_id','$fee_plan','$receipt_no','$booklet_no','$fee_date','$due_date')";
	$add_fee_result = mysql_query($add_fee_query) or die("ERROR 2 : ".mysql_error());

	$add_fee_history_query = "INSERT INTO sc_fee_history(fee_history_id,history_member_id,fee_plan,fee_amount,fee_receipt_number,fee_booklet_number,renew_date,time,due_date,admin_id) 
	VALUES(NULL,'$member_id','$fee_plan','$fee_amount','$receipt_no','$booklet_no','$fee_date','$current_time','$due_date','$admin_id')";
	$add_fee_history_query = mysql_query($add_fee_history_query) or die("ERROR 3 : ".mysql_error());

	$fee_flag_query = "UPDATE sc_member_flag SET fee_flag='1' WHERE member_flag_id = '$member_id'";
	$fee_flag_result = mysql_query($fee_flag_query) or die("ERROR 4 : ".mysql_error());

	$update_member_status = "UPDATE sc_member SET member_status='1' WHERE member_id = '$member_id'";
	$fee_member_status = mysql_query($update_member_status) or die("ERROR 5 : ".mysql_error());

	header('Location: ../member_entry.php?member_id='.$member_id.'&in=-1');
}
?>