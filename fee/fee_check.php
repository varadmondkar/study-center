<?php 
session_start(); 
if(!isset($_SESSION['admin_id']))
{
	header('Location: http://localhost/study_center/');
}
include '../db_config/db_config.php';

date_default_timezone_set("Asia/Calcutta");
$member_id=$_GET["id"];

// Unsetting the fee flag
$fee_due_date_query = "SELECT fee_due_date FROM sc_fee WHERE fee_member_id='$member_id'";
$fee_due_date_result = mysql_query($fee_due_date_query) or die("ERROR 01 : ".mysql_error());
$member_fee_row_count = mysql_num_rows($fee_due_date_result);
while($rows=mysql_fetch_array($fee_due_date_result))
	extract($rows);

// If new member enters than he/she does not have any entry in sc_fee and sc_fee_history hence no due_date so check member fee row count
if($member_fee_row_count!=0)
{
	//$fee_due_date=strtotime($fee_due_date);
	$today_date=date("Y-m-d");
	//$today_date=strtotime($today_date);
	
	if($today_date > $fee_due_date)
	{
		$update_fee_flag_query = "UPDATE sc_member_flag SET fee_flag=0 WHERE member_flag_id='$member_id'";
		$update_fee_flag = mysql_query($update_fee_flag_query) or die("ERROR 02 : ".mysql_error());
		$update_member_status_query = "UPDATE sc_member SET member_status=0 WHERE member_id='$member_id'";
		$update_member_status_result = mysql_query($update_member_status_query) or die("ERROR 03 : ".mysql_error());
	}
}
// End of Unsetting the fee flag

$select = "SELECT * FROM sc_member WHERE member_id='$member_id'";
$result = mysql_query($select) or die("ERROR : ".mysql_error());
$member_count = mysql_num_rows($result);
if($member_count==1)
{
	$query1 = "SELECT member_status FROM sc_member WHERE member_id='".$member_id."'";
	$result1 = mysql_query($query1) or die("ERROR 1 : ".mysql_error());
	while($rows=mysql_fetch_array($result1))
		extract($rows);

	$query2 = "SELECT fee_flag FROM sc_member_flag WHERE member_flag_id='".$member_id."'";
	$result2 = mysql_query($query2) or die("ERROR 2 : ".mysql_error());
	while($rows=mysql_fetch_array($result2))
		extract($rows);

	if($member_status==0)
	{
		$query3 = "SELECT fee_due_date FROM sc_fee WHERE fee_member_id='$member_id'";
		$result3 = mysql_query($query3) or die("ERROR 3 : ".mysql_error());
		$member_fee_count = mysql_num_rows($result3);

		if($fee_flag==0 AND $member_fee_count==0)
		{
			echo "<div id='member_fee_form'>";
			echo "<br/><br/>";
			echo "<font size='+1'>This is the New Member. Fill the following details to pay his/her Fees.</font>";
			echo "<br/>";
			echo "</div>";
?>
		<form name="member_fee_form" id="member_fee_form" action="fee/confirm_fee.php" method="POST" onsubmit="return validate();">
			<table border="0" align="center">
				<tr>
					<td><br/>Member Name:</td>
					<td><br/>
					<?php 
					$member_name_query = "SELECT member_name FROM sc_member WHERE member_id='$member_id'";
					$member_name_result = mysql_query($member_name_query);
					while($rows=mysql_fetch_array($member_name_result))
						extract($rows);
					?>
					<input type="hidden" name="member_id" value="<?php echo $member_id; ?>" />
					<input type="text" name="member_name" value="<?php echo $member_name; ?>" readonly/>
					</td>
				</tr>
				<tr>
					<td><br/>Fee Plan:</td>
					<td><br/>
					<?php 
					$fee_structure_query = "SELECT * FROM sc_fee_structure";
					$fee_structure_result = mysql_query($fee_structure_query);
					?>
						<select name="fee_plan">
							<option value='0'>Fee plan</option>
							<?php
							while ($fee = mysql_fetch_array($fee_structure_result))
							{
								extract($fee);
								echo "<option value='".$fee_duration."'>".$fee_duration."</option>";
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><br/>Fee Booklet No:</td>
					<td><br/><input type="text" name="booklet_no" placeholder=" Fee Booklet Number" onkeypress="return isNumber(event)" /></td>
				</tr>
				<tr>
					<td><br/>Fee Receipt No:</td>
					<td><br/><input type="text" name="receipt_no" placeholder=" Fee Receipt Number" onkeypress="return isNumber(event)" /></td>
				</tr>			
				<tr>
					<td><br/>Date (Fee payment date):</td>
					<td><br/><input type="text" name="fee_date" placeholder=" yyyy-mm-dd" /></td>
				</tr> <!-- Added for time being -->
				<tr>
					<td colspan="4" style="text-align: center;"><br/><input type="submit" value="Register" /></td>
				</tr>
			</table>
		</form>
<?php
		}

		if($fee_flag==0 AND $member_fee_count==1)
		{
			echo "<div id='member_fee_form'>";
			echo "<br/><br/>";
			echo "<font size='+1'>Member fees are due. Fill the following details to pay his/her Fees.</font>";
			echo "<br/>";
			echo "</div>";
?>
		<form name="member_fee_form" id="member_fee_form" action="fee/confirm_renew_fee.php" method="POST" onsubmit="return validate();">
			<table border="0" align="center">
				<tr>
					<td><br/>Member Name:</td>
					<td><br/>
					<?php 
					$member_name_query = "SELECT member_name FROM sc_member WHERE member_id='$member_id'";
					$member_name_result = mysql_query($member_name_query);
					while($rows=mysql_fetch_array($member_name_result))
						extract($rows);
					?>
					<input type="hidden" name="member_id" value="<?php echo $member_id; ?>" />
					<input type="text" name="member_name" value="<?php echo $member_name; ?>" readonly/>
					</td>
				</tr>
				<?php 
				$last_fee_plan_query = "SELECT * FROM sc_fee WHERE fee_member_id='$member_id'";
				$last_fee_plan_result = mysql_query($last_fee_plan_query);
				while($rows=mysql_fetch_array($last_fee_plan_result))
					extract($rows);
				?>
				<tr>
					<td><br/>Last Fee Plan:</td>
					<td><br/>
					<input type="text" name="last_fee_plan" value="<?php echo $fee_plan; ?>" readonly/>
					</td>
				</tr>
				<tr>
					<td><br/>Last Due Date:</td>
					<td><br/>
					<input type="text" name="last_due_date" value="<?php echo $fee_due_date; ?>" readonly/>
					</td>
				</tr>
				<tr><td colspan='2'><br/><hr><br/></td></tr>
				<tr>
					<td><br/>Fee Plan:</td>
					<td><br/>
					<?php 
					$fee_structure_query = "SELECT * FROM sc_fee_structure";
					$fee_structure_result = mysql_query($fee_structure_query);
					?>
						<select name="fee_plan">
							<option value='0'>Fee plan</option>
							<?php
							while ($fee = mysql_fetch_array($fee_structure_result))
							{
								extract($fee);
								echo "<option value='".$fee_duration."'>".$fee_duration."</option>";
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><br/>Fee Booklet No:</td>
					<td><br/><input type="text" name="booklet_no" placeholder=" Fee Booklet Number" onkeypress="return isNumber(event)" /></td>
				</tr>
				<tr>
					<td><br/>Fee Receipt No:</td>
					<td><br/><input type="text" name="receipt_no" placeholder=" Fee Receipt Number" onkeypress="return isNumber(event)" /></td>
				</tr>		
				<tr>
					<td><br/>Date (Fee payment date):</td>
					<td><br/><input type="text" name="fee_date" placeholder=" yyyy-mm-dd" /></td>
				</tr> <!-- Added for time being -->	
				<tr>
					<td colspan="4" style="text-align: center;"><br/><input type="submit" value="Register" /></td>
				</tr>
			</table>
		</form>
<?php
		}
	}
	elseif($member_status==1)
	{
		
		$query4 = "SELECT * from sc_fee where fee_member_id='$member_id'";
		$result4 = mysql_query($query4) or die("ERROR 6 : ".mysql_error());
		while($rows=mysql_fetch_array($result4))
			extract($rows);
		
		$query5 = "SELECT * FROM sc_member WHERE member_id='$member_id'";
		$result5 = mysql_query($query5) or die("ERROR : ".mysql_error());
		while($rows=mysql_fetch_array($result5))
			extract($rows);

		echo "<div id='member_fee_form'>";
		echo "<br/><br/>";
		echo "<font size='+1'>Member has paid the fees. Member details are as follows:</font>";
		echo "<br/><br/>";
		echo "<table border='0'>";
		echo "<tr><td>";
		echo "<input type='hidden' name='member_id' value='".$member_id."' readonly><br>";
		echo "<input type='hidden' name='identifier' value='old'>";	
		echo "</td></tr>";
		echo "<tr><td>";
		echo "<label class='description'><font size='3'>Member Name:</font></label>";
		echo "</td><td>";
		echo "<input type='text' name='member_name' value='".$member_name."' readonly><br>";
		echo "</td></tr>";
		echo "<tr><td>";
		echo "<label class='description'><font size='3'>Plan:</font></label>";
		echo "</td><td>";
		echo "<input type='text' name='plan' value='".$fee_plan."' readonly><br>";
		echo "</td></tr>";
		echo "<tr><td>";
		echo "<label class='description'><font size='3'>Payment Date:</font></label>";
		echo "</td><td>";
		echo "<input type='text' name='payment_date' value='".$fee_payment_date."' readonly><br>";
		echo "</td></tr>";
		echo "<tr><td>";
		echo "<label class='description'><font size='3'>Due Date:</font></label>";
		echo "</td><td>";
		echo "<input type='text' name='due_date' value='".$fee_due_date."' readonly><br>";
		echo "</td></tr>";
		echo "<tr><td>";
		echo "<label class='description'><font size='3'>Receipt Number:</font></label>";
		echo "</td><td>";
		echo "<input type='text' name='receipt' value='".$fee_receipt_number."' readonly><br>";
		echo "</td></tr>";
		echo "<tr><td>";
		echo "<label class='description'><font size='3'>Booklet Number:</font></label>";
		echo "</td><td>";
		echo "<input type='text' name='booklet' value='".$fee_booklet_number."' readonly><br>";
		echo "</td></tr>";
		echo "</table>";
		echo "</div>";
	}

	else
	{
		$lockreason = mysql_query("SELECT lock_reason FROM sc_member_flag WHERE member_flag_id='$member_id'")  or die("ERROR : ".mysql_error());
		while($rows=mysql_fetch_array($lockreason))
			extract($rows);
		
		echo "<div id='member_fee_form'>";
		echo "<br/><br/>";
		echo "<font size='+1'>Sorry Member is Locked for the following reason:<font>";
		echo "<br/><br/><br/>";
		echo "<textarea>$lock_reason</textarea>";
		echo "<br/><br/>";
		echo "</div>";
	}
}
else
{
	echo "<br/><br/><center>";
	echo "<font size='+1'>Member does not exist. Please check the Member Id.</font>";
	echo "<br/><br/></center>";
}

?>